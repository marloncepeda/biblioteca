-- phpMyAdmin SQL Dump
-- version 2.11.9.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 06-12-2014 a las 03:17:02
-- Versión del servidor: 5.0.67
-- Versión de PHP: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Base de datos: `biblioteca`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sv_configuracion`
--

CREATE TABLE IF NOT EXISTS `sv_configuracion` (
  `id` int(255) NOT NULL auto_increment,
  `nombre` varchar(100) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `telefono` varchar(14) NOT NULL,
  ` correo` varchar(150) NOT NULL,
  `facebook` varchar(150) NOT NULL,
  `twitter` varchar(150) NOT NULL,
  `url_logo` varchar(150) NOT NULL,
  `url_fondo` varchar(200) NOT NULL,
  `autor` int(255) NOT NULL,
  `fecha_registro` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcar la base de datos para la tabla `sv_configuracion`
--

INSERT INTO `sv_configuracion` (`id`, `nombre`, `direccion`, `telefono`, ` correo`, `facebook`, `twitter`, `url_logo`, `url_fondo`, `autor`, `fecha_registro`) VALUES
(1, 'Biblioteca Publica Andrés Eloy Blanco', 'Barinas Estado Barinas  Centro de Barinas con Avenida Brinceño Méndez cruce con Calle Carvaja', '0273-215-09-02', 'bibliotecapublica-aeb@hotmail.com', '', '@BPAndresEloyBlanco', 'http://localhost/biblioteca/www/lib/themes/biblioteca/img/logo.png', '', 0, '19-07-2014');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sv_dewey`
--

CREATE TABLE IF NOT EXISTS `sv_dewey` (
  `id` int(255) NOT NULL auto_increment,
  `codigo` varchar(3) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `descripcion` text NOT NULL,
  `autor` int(255) NOT NULL,
  `fecha_registro` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Volcar la base de datos para la tabla `sv_dewey`
--

INSERT INTO `sv_dewey` (`id`, `codigo`, `nombre`, `descripcion`, `autor`, `fecha_registro`) VALUES
(1, '00', 'Obras Generales', 'Obras Generales', 0, '19-07-2014'),
(2, '100', 'Filosofía y Psicología', 'Filosofía y Psicología', 0, '19-07-2014'),
(3, '200', 'Religión', 'Religión', 0, '19-07-2014'),
(4, '300', 'Ciencias Sociales', 'Ciencias Sociales (Sociología,  Política, Economía, Derecho, Premilitar, Educación, Folklore, Ambiente)', 0, '19-07-2014'),
(5, '400', 'Lingüística', 'Lingüística (Ingles)', 0, '19-07-2014'),
(6, '500', 'Ciencias Puras', 'Ciencias Puras (Matemática, Física, Química, Geología, Ciencia de la Tierra, Biología, Botánica, Salud, Dibujo Técnico, Medicina, Agricultura)', 0, '19-07-2014'),
(7, '600', 'Ciencia Aplicada', 'Ciencia Aplicada (Administración)', 0, '19-07-2014'),
(8, '700', 'Arte y Recreación', 'Arte y Recreación', 0, '19-07-2014'),
(9, '800', 'Literatura', 'Literatura (Novelas, Novelas de Venezuela,  Poesía, Poesía de Venezuela, Teatro, Teatro de Venezuela)', 0, '19-07-2014'),
(10, '900', 'Geografía', 'Geografía (Historia, Educación Artística, Geografía General, Geografía de Venezuela, Catedra de Bolivariana, Historia de Venezuela, Historia Universal)', 0, '19-07-2014');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sv_informacion`
--

CREATE TABLE IF NOT EXISTS `sv_informacion` (
  `id` int(255) NOT NULL auto_increment,
  `nombre` varchar(255) NOT NULL,
  `descripcion` text NOT NULL,
  `tipo` varchar(20) NOT NULL,
  `autor` int(255) NOT NULL,
  `fecha` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Volcar la base de datos para la tabla `sv_informacion`
--

INSERT INTO `sv_informacion` (`id`, `nombre`, `descripcion`, `tipo`, `autor`, `fecha`) VALUES
(1, 'Historia', 'Barinas forma parte de la red de bibliotecas públicas desde 1977, para el mes de mayo se crea la red de bibliotecas públicas a través de decreto, y se funda la biblioteca pública “Andrés Eloy Blanco” y es puesta en funcionamiento para el público en general, en Enero de 1978.  Andrés Eloy Blanco es una institución pública perteneciente a la red de bibliotecas públicas del país, adscrita a la Secretaria Ejecutiva De Educación, bajo los lineamientos funcionales de biblioteca nacional, con el objetivo de seleccionar y organizar los textos bibliográficos que a ella llegan (Compra- Donación); con el fin de ponerlos a disposición del público en general, para que, todos los sectores tengan acceso a ella, y así cumplir con un derecho ciudadano que el estado garantiza.', 'Publicidad', 1, '29-05-2014'),
(2, '¿Para que Sirve?', 'Mediante este servicio, la Biblioteca te permite disponer de sus fondos bibliográficos, para su uso fuera de sus instalaciones por un periodo de tiempo determinado. Los usuarios podrán acceder a este servicio registrandose en la pagina y asi entregarles su carne que les acredite como tales', 'Publicidad', 1, '29-05-2014'),
(3, '¿Quien tiene Acceso?', '\r\n         Todo los usuarios con carné de la Biblioteca actualizado', 'publicidad', 1, ''),
(4, 'Condiciones del Prestamo', 'Los prestamos tendran un limite de 3 Dias sin execcion y solo si existen mas de 3 ejemplares\r\n  Si el libro de tu interes no esta disponible puedes reservarlo y cuando haya la disponibilidad se le avisará a través de mensaje de texto o correo electronico que te hayas registrado comprueba en los mostradores de información y préstamo que tenemos tu e-mail en nuestra base de datos', 'publicidad', 1, '29-05-2014'),
(5, 'Penalizacion', 'Una vez vencido el plazo de devolución, si la obra no ha sido devuelta, la autorización para nuevos préstamos queda suspendida. Se informará al usuario mediante un correo electrónico o mensaje de texto.\r\nLa penalización por la devolución de obras con retraso es de 1 mes de suspensión de carnet de prestamos \r\nSi la obra no ha sido devuelta después de tres avisos de reclamación, se cancelará el acceso del usuario a la Biblioteca hasta la devolución de la obra en préstamo.\r\nLa pérdida o deterioro de un libro supone la desautorización de nuevos préstamos hasta la reposición del mismo o, en su defecto, el abono de su importe.\r\nEl carné es personal e intransferible, por lo que su utilización por otra persona será sancionado.', 'publicidad', 1, '29-05-2014'),
(7, 'Vision ', 'la vision es X Y Z', 'Vision', 1, '29-09-2014'),
(9, 'Mision', 'la mision va aca y tiene 2000 caracteres para llenar', 'Mision', 1, '13-10-2014');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sv_libros`
--

CREATE TABLE IF NOT EXISTS `sv_libros` (
  `id` int(255) NOT NULL auto_increment,
  `id_dewey` int(255) NOT NULL,
  `id_dewey_decimal` varchar(3) NOT NULL,
  `img_ruta` varchar(100) NOT NULL,
  `img_ruta2` varchar(255) NOT NULL,
  `img_ruta3` varchar(255) NOT NULL,
  `img_ruta4` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `tags` varchar(255) NOT NULL,
  `autor` varchar(255) NOT NULL,
  `cantidad` int(2) NOT NULL,
  `usuario_id` int(255) NOT NULL,
  `fecha_registro` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `id_dewey` (`id_dewey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `sv_libros`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sv_logs`
--

CREATE TABLE IF NOT EXISTS `sv_logs` (
  `id` int(255) NOT NULL auto_increment,
  `ci_user` varchar(22) NOT NULL,
  `accion` varchar(35) NOT NULL,
  `fecha_registro` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `sv_logs`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sv_mensajes_internos`
--

CREATE TABLE IF NOT EXISTS `sv_mensajes_internos` (
  `id` int(255) NOT NULL auto_increment,
  `nombres` varchar(30) collate utf8_bin NOT NULL,
  `email` varchar(150) collate utf8_bin NOT NULL,
  `telefono` varchar(14) collate utf8_bin NOT NULL,
  `mensaje` text collate utf8_bin NOT NULL,
  `estado` varchar(15) collate utf8_bin NOT NULL,
  `fecha` varchar(12) collate utf8_bin NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `sv_mensajes_internos`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sv_plantillas`
--

CREATE TABLE IF NOT EXISTS `sv_plantillas` (
  `id` int(255) NOT NULL auto_increment,
  `id_user` int(255) NOT NULL,
  `tipo` varchar(15) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `descripcion` varchar(140) NOT NULL,
  `fecha_registro` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `sv_plantillas`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sv_prestamos`
--

CREATE TABLE IF NOT EXISTS `sv_prestamos` (
  `id` int(255) NOT NULL auto_increment,
  `id_user` int(255) NOT NULL,
  `id_libro` int(255) NOT NULL,
  `id_bibliotecario` int(255) NOT NULL,
  `status` varchar(10) NOT NULL,
  `cant_dias_prestamo` varchar(2) NOT NULL,
  `fecha_prestamo_retorno` varchar(10) NOT NULL,
  `fecha_registro` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `id_libro` (`id_libro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `sv_prestamos`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sv_users`
--

CREATE TABLE IF NOT EXISTS `sv_users` (
  `id` int(255) NOT NULL auto_increment,
  `nacionalidad` varchar(1) NOT NULL,
  `ci` varchar(27) NOT NULL,
  `nombre` varchar(15) NOT NULL,
  `apellido` varchar(15) NOT NULL,
  `usuario` varchar(15) NOT NULL,
  `clave` varchar(21) NOT NULL,
  `fecha_nac` varchar(10) NOT NULL,
  `tipo_user` varchar(15) NOT NULL,
  `estado` varchar(10) NOT NULL,
  `correo` varchar(150) NOT NULL,
  `telefono` varchar(12) NOT NULL,
  `ruta_img` varchar(200) NOT NULL,
  `pregunta` varchar(30) NOT NULL,
  `respuesta` varchar(30) NOT NULL,
  `fecha_registro` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `usuario` (`usuario`),
  UNIQUE KEY `correo` (`correo`),
  UNIQUE KEY `ci` (`ci`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=107 ;

--
-- Volcar la base de datos para la tabla `sv_users`
--

INSERT INTO `sv_users` (`id`, `nacionalidad`, `ci`, `nombre`, `apellido`, `usuario`, `clave`, `fecha_nac`, `tipo_user`, `estado`, `correo`, `telefono`, `ruta_img`, `pregunta`, `respuesta`, `fecha_registro`) VALUES
(1, 'V', '00.000.017', 'nombres', 'apellidos', 'admin', '123456', '02-02-1990', 'admin', 'activo', 'warlhrs@gmail.com', '0412', 'IMG_20141116_033909.jpg', 'cual es mi gato', 'calesita', '28-04-2014'),
(5, 'V', '00.000.003', 'mario', 'cegobia', 'bibliotecario', 'bibliotecario', '10-09-1989', 'bibliotecario', 'activo', 'mario@biblioteca.com', '', 'Captura de pantalla de 2014-10-01 11:35:08.png', '', '', '29-05-2014'),
(103, 'V', '00.000.013', 'usuario', 'usuario', 'usuario', 'usuario', '1983-12-31', 'users', 'activo', 'usuario@biblioteca.com', '584245713467', 'descarga.jpg', '', '', '13-10-2014'),
(104, 'V', '00.000.014', 'oso', 'oso', 'oso', '123456', '1991-10-01', 'users', 'activo', 'as@biblioteca.com', '0423-542-121', '', '', '', '13-10-2014'),
(106, 'V', '21.056.713', 'laura', 'pascualis', 'laura', '123456', '2000-12-31', 'users', 'activo', 'laura@biblioteca.com', '04121539069', '', '', '', '14-10-2014');

--
-- Filtros para las tablas descargadas (dump)
--

--
-- Filtros para la tabla `sv_libros`
--
ALTER TABLE `sv_libros`
  ADD CONSTRAINT `sv_libros_ibfk_1` FOREIGN KEY (`id_dewey`) REFERENCES `sv_dewey` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `sv_prestamos`
--
ALTER TABLE `sv_prestamos`
  ADD CONSTRAINT `sv_prestamos_ibfk_1` FOREIGN KEY (`id_libro`) REFERENCES `sv_libros` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
