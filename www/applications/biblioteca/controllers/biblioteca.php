<?php
/**
 * Access from index.php:
 */
class biblioteca_Controller extends ZP_Controller {
	
	public function __construct() {
		$this->app("biblioteca");
		$this->Files = $this->core("Files");
		$this->Templates = $this->core("Templates");
		$this->Templates->theme();
		$this->helper("alerts");
		$this->helper("sessions");
		$this->biblioteca_Model = $this->model("biblioteca_Model");
	}
	//**funciones del menu principal del sistema**

	//index es la funcion principal nos abre la primera
	//pantalla de nuestro website y nos muestra el catalogo
	//de productos
	public function index() {
		if( (SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="user") ){
			$this->home();
		}else{
			
			$this->frontend(); 
		}
	}
	
	public function frontend(){
		$this->title("Inicio");
		$vars["publicidad"]  = $this->biblioteca_Model->informacion_status('Publicidad');
		$vars["mision"]  = $this->biblioteca_Model->informacion_status('Mision');
		$vars["vision"]  = $this->biblioteca_Model->informacion_status('Vision');
		$vars["ultimos_libros"]  = $this->biblioteca_Model->libros_ultimos("3");
		$vars["mas_vistos_libros"]  = $this->biblioteca_Model->libros_ultimos("13");
		$vars["dewey"]  = $this->biblioteca_Model->dewey_todos();
		$vars["quienes_somos"]  = $this->biblioteca_Model->datos_ubicacion();
		$vars["informacion_ubicacion"]  = $this->biblioteca_Model->datos_ubicacion();
		$vars["view"] = $this->view("frontend/frontend",TRUE);
		$this->render("content",$vars);
	}
	public function ver_catalogo(){
		$this->title("Libros");
		$vars["publicidad"]  = $this->biblioteca_Model->informacion_status('Publicidad');
		$vars["mision"]  = $this->biblioteca_Model->informacion_status('Mision');
		$vars["vision"]  = $this->biblioteca_Model->informacion_status('Vision');
		$vars["ultimos_libros"]  = $this->biblioteca_Model->libros_ultimos("3");
		$vars["mas_vistos_libros"]  = $this->biblioteca_Model->libros_ultimos("13");
		$vars["dewey"]  = $this->biblioteca_Model->dewey_todos();
		$vars["quienes_somos"]  = $this->biblioteca_Model->datos_ubicacion();
		$vars["informacion_ubicacion"]  = $this->biblioteca_Model->datos_ubicacion();
		$vars["view"] = $this->view("frontend/catalogo",TRUE);
		$this->render("content",$vars);
	}
	public function buscar_libro_por(){
		$this->title("Buscar Libro");
		$vars["publicidad"]  = $this->biblioteca_Model->informacion_status('Publicidad');
		$vars["mision"]  = $this->biblioteca_Model->informacion_status('Mision');
		$vars["vision"]  = $this->biblioteca_Model->informacion_status('Vision');
		$vars["ultimos_libros"]  = $this->biblioteca_Model->libros_buscar_por($_GET["letra"]);
		$vars["dewey"]  = $this->biblioteca_Model->dewey_todos();
		$vars["quienes_somos"]  = $this->biblioteca_Model->datos_ubicacion();
		$vars["informacion_ubicacion"]  = $this->biblioteca_Model->datos_ubicacion();
		$vars["view"] = $this->view("frontend/catalogo",TRUE);
		$this->render("content",$vars);
	}
	public function buscar_libro_espesifico(){
		$this->title("Buscar Libro");
		$vars["publicidad"]  = $this->biblioteca_Model->informacion_status('Publicidad');
		$vars["mision"]  = $this->biblioteca_Model->informacion_status('Mision');
		$vars["vision"]  = $this->biblioteca_Model->informacion_status('Vision');
		$vars["ultimos_libros"]  = $this->biblioteca_Model->libros_buscar_espesifico($_GET["letra"]);
		$vars["dewey"]  = $this->biblioteca_Model->dewey_todos();
		$vars["quienes_somos"]  = $this->biblioteca_Model->datos_ubicacion();
		$vars["informacion_ubicacion"]  = $this->biblioteca_Model->datos_ubicacion();
		$vars["view"] = $this->view("frontend/catalogo",TRUE);
		$this->render("content",$vars);
	}
	//Home es la funcion que nos muestra el menu de administracion
	//una vez logueado en nustro website
	public function home() {	
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="bibliotecario")){

			$data = $this->biblioteca_Model->libros_todos();
			$data1 = $this->biblioteca_Model->libros_prestados_usuario_prestados(SESSION('id'),"Prestado");
			
			if($data == false){
				$this->title("Todos los libros prestados");
				echo "<script>alert('No Hay Libros Prestados');</script>";
				$vars["view"] = $this->view("home",TRUE);
				$this->render("content",$vars);
			}else{
				$this->title("Todos los libros prestados");
				$vars["libros"] = $data;
				$vars["prestamos"] = $data1;
				$vars["view"] = $this->view("home",TRUE);
				$this->render("content",$vars);
				
				$logs = $this->biblioteca_Model->logs("Ver Libros Prestados");
			}
		}else{
			$this->login(); 
		}
	}
	//Salir es la funcion que elimina las sessiones usadas en el site
	//y nos redirig a la pantalla principal del site 
	public function salir() {	
		$logs = $this->biblioteca_Model->logs("Salir");		
 		unsetSessions($URL);
		$this->login(); 
	}

	public function perfil_carnet(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="bibliotecario")){

				$foto =get("webURL")."/www/lib/themes/biblioteca/images/".SESSION('ruta_img');
				$ruta_carnet = get("webURL")."/www/lib/themes/biblioteca/images/carnet.jpg";
				Header("Content-type: image/jpeg; charset=utf-8'");
			    // Seteamos las dos imágenes con las que trabajaremos 
				$avatar = imagecreatefromjpeg($ruta_carnet); 
				$no = imagecreatefromjpeg($foto); 
				//Creamos el color de las letras
				$textcolor = imagecolorallocate($avatar, 0, 0, 0); 
				
				//Las unimos las imagenes y agregamos el texto 
				imagecopyresampled($avatar,$no,248,82,0,0,imagesx($avatar),imagesy($avatar),imagesx($avatar),imagesy($avatar)); 
				imagestring($avatar, 3, 112, 108, SESSION("nombre").", ".SESSION("apellido"), $textcolor);
				imagestring($avatar, 5, 55, 137, SESSION("ci"), $textcolor);
				imagestring($avatar, 5, 62, 166, SESSION("telefono"), $textcolor);
				imagestring($avatar, 3, 15, 196, "Fecha Expedicion: 2014", $textcolor);
				//imagestring($avatar, 5, 65, 193, "Direccion", $textcolor); 
				
				//Salvamos el resultado en JPEG 
				Imagejpeg($avatar); 
				//Liberamos memoria 
				imagedestroy($avatar); 
				imagedestroy($no);
	    }else{
			$this->login(); 
		}
	}
	public function perfil_usuario(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="bibliotecario")){
			if(POST("modificar")){
				//$uploaddir = 'c:\xampp/htdocs/biblioteca/www/lib/themes/biblioteca/images/';
				$uploaddir = '/var/www/biblioteca/www/lib/themes/biblioteca/images/';
				$uploadfile = $uploaddir.basename($_FILES['userfile']['name']);
				$namefile = basename($_FILES['userfile']['name']);

				if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
					chmod($uploadfile, 0755);
					$data = $this->biblioteca_Model->modificar_perfil(SESSION("id"),$namefile);
				
					echo "<script>alert('Datos Guardados con Exito Vuelva a Entrar al Sistema');</script>";
					$logs = $this->biblioteca_Model->logs("Modificar Perfil");
					$this->salir();   	
				} else {
					echo "<script>alert('Error en la carga del archivo'".basename($_FILES['userfile']['name'])."')</script>";
					$this->perfil_usuario();
				}

			}else{
				$data = $this->biblioteca_Model->buscar_logs(SESSION("ci"));
				//____($data);
				$vars["logs"] = $data;
				$this->title("Ver Contactos");
				$vars["view"] = $this->view("perfil",TRUE);
				$this->render("content",$vars);
				$logs = $this->biblioteca_Model->logs("Ver Perfil");

			}
		}else{
			$this->login(); 
		}
	}


	public function datos_ver(){
		if(SESSION("tipo_user")=="admin"){
			$data = $this->biblioteca_Model->datos_ubicacion();	
			$vars["datos_ubicacion"] = $data;
			$vars["view"] = $this->view("configuracion/datos_ubicacion",TRUE);
			$this->render("content",$vars);
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}

	public function datos_agregar(){
		if(SESSION("tipo_user")=="admin"){
			if(POST("agregar")){
				$this->biblioteca_Model->datos_agregar();
				//____($data);
				echo "<script>alert('Se ha guardado Correctamente');</script>";
				$this->listas_informacion();
			}else{
				$this->listas_informacion();
			}
		}else{
			$this->login();
		}	
	}
	public function datos_modificar(){
		if(SESSION("tipo_user")=="admin"){
			if(POST("modificar")){
				$data = $this->biblioteca_Model->datos_modificar(POST("datos_id"));
				$this->listas_informacion();
			}
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}
	public function listas_informacion() {
		if(SESSION("tipo_user")=="admin"){
			$data = $this->biblioteca_Model->buscar_todos_informacion();
			if($data =="false"){
				$this->informacion_agregar();
			}else{
				$this->title("Lista Informacion");
				$vars["informacion"] = $data;
				$vars["view"] = $this->view("configuracion/informacion",TRUE);
				$this->render("content",$vars);
				$logs = $this->biblioteca_Model->logs("Ver Listas de Informacion");
			}
		}else{
			$this->login(); 
		}
	}
	public function informacion_agregar(){
		if(SESSION("tipo_user")=="admin"){
			if(POST("agregar")){
				$this->biblioteca_Model->informacion_agregar();
				//____($data);
				echo "<script>alert('Se ha guardado Correctamente');</script>";
				$this->listas_informacion();
			}else{
				$this->listas_informacion();
			}
		}else{
			$this->login();
		}	
	}
	public function recuperar_clave(){
			if(POST("registro")){
				$data = $this->biblioteca_Model->buscar_pregunta_usuario(POST("correo"));
				if($data[0]["respuesta"]==POST("respuesta")){
					$id=$data[0]["id"];
					$this->biblioteca_Model->modificar_clave($id);
					echo "<script>alert('Se ha Modificado Correctamente');</script>";
					$this->login();
				}else{
					echo "<script>alert('La respuesta no es la misma');</script>";
				}
			}else{
				$this->title("Recuperar Clave");
				$vars["view"] = $this->view("recuperar",TRUE);
				$this->render("content",$vars);
			}	
	}
	public function informacion_modificar(){
		if(SESSION("tipo_user")=="admin"){
			if(POST("modificar")){
				$data = $this->biblioteca_Model->info_modificar(POST("info_id"));
				$this->listas_informacion();
			}
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}
	public function informacion_borrar(){
		if(SESSION("tipo_user")=="admin"){
			if(POST("borrar")){
				$data = $this->biblioteca_Model->info_borrar(POST("info_id"));
				$this->listas_informacion();
			}
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}
	public function registrar(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="bibliotecario")){
			$this->home();
		}else{
			if(POST("registro")){
				$data = $this->biblioteca_Model->registrar();
				//____($data);
				echo "<script>alert('Se ha guardado Correctamente');</script>";
				$this->login();
			}else{
				$this->title("Registro");
				$vars["view"] = $this->view("registrar",TRUE);
				$this->render("content",$vars);
			}
		}
	}
	public function registrar_int(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="bibliotecario")){
			if(POST("registro")){
				$data = $this->biblioteca_Model->registrar();
				//____($data);
				echo "<script>alert('Se ha guardado Correctamente');</script>";
				$this->login();
			}else{
				$this->title("Registro");
				$vars["view"] = $this->view("registrar",TRUE);
				$this->render("content",$vars);
			}
		}else{
			$this->login();
		}
	}
	//Login es la funcion encargada de manipular el acceso al sistema
	public function login() {
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="bibliotecario")){
			$this->home();
		}else{
			if(POST("entrar")){
				$data = $this->biblioteca_Model->login();
				//____($data);
				if($data == false){
					echo "<script>alert('no tiene cuenta creada'); document.location.href='';</script>";
					echo "<script>document.location.href='http://localhost/biblioteca';</script>";
				}else{
					if($data[0]["estado"]=="bloqueado"){
						echo "<script>alert('Tu Usuario esta Bloqueado Contacta con tu Administrador');</script>";
						echo "<script>document.location.href='http://localhost/biblioteca';</script>";
						//$this->salir();
					}else{
						SESSION("tipo_user", $data[0]["tipo_user"]);
						SESSION("apellido", $data[0]["apellido"]);
						SESSION("nombre", $data[0]["nombre"]);
						SESSION("id", $data[0]["id"]);
						SESSION("fecha_user", $data[0]["fecha_nac"]);
						SESSION("correo", $data[0]["correo"]);
						SESSION("ci", $data[0]["ci"]);
						SESSION("ruta_img", $data[0]["ruta_img"]);
						SESSION("telefono", $data[0]["telefono"]);

						$logs = $this->biblioteca_Model->logs("Entrar");
						
						
						$this->home();
					}
				}
			}elseif(POST("registrar")){
				$this->registrar();
			}else{
				//SESSION("tipo_user","login");
				$this->title("Inicio");
				$this->CSS("login", "biblioteca");
				$vars["view"] = $this->view("login",TRUE);
				$this->render("content",$vars);
			}
		}
	}
	
	public function listas_usuarios() {
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="bibliotecario")){
			$data = $this->biblioteca_Model->buscar_todos_usuarios();
			//____($data);
			if($data == false){
				echo "<script>alert('no hay usuarios en el sistema'); document.location.href='';</script>";
			}else{
				$this->title("Lista Usuarios");
				$vars["usuarios"] = $data;
				$vars["view"] = $this->view("contactos/todos_lista_global",TRUE);
				$this->render("content",$vars);
				$logs = $this->biblioteca_Model->logs("Ver Listas de Usuarios");
			}
		}else{
			$this->login(); 
		}
	}

	public function usuarios_options(){
		if( (SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="bibliotecario")){
			if(POST("borrar")){
				$data = $this->biblioteca_Model->usuario_borrar(POST("libro_id"));
				echo "<script>alert('usuario Borrado con exito');</script>";
				$logs = $this->biblioteca_Model->logs("Borrar Usuario");	
				$this->listas_usuarios();
			}elseif(POST("bloquear")){
				$data = $this->biblioteca_Model->usuario_estado(POST("libro_id"),"bloqueado");
				$logs = $this->biblioteca_Model->logs("Bloquear Usuario");
				$this->listas_usuarios();
			}elseif(POST("desbloquear")){
				$data = $this->biblioteca_Model->usuario_estado(POST("libro_id"),"activo");
				$logs = $this->biblioteca_Model->logs("Desbloquear Usuario");
				$this->listas_usuarios();
			}
		}else{
			$this->login(); 
		}
	}
	public function usuario_borrar(){
		if(SESSION("tipo_user")=="admin"){
			if(POST("borrar")){
				$data = $this->biblioteca_Model->usuario_borrar(POST("archivo_id"));
				//____($data);
				$logs = $this->biblioteca_Model->logs("borrar contacto");
				echo "<script>alert('Se ha borrrado Correctamente');</script>";
				$this->listas_usuarios();
			}else{
				$this->title("Ver Contactos");
				$vars["view"] = $this->view("contactos/home",TRUE);
				$this->render("content",$vars);
			}
		}else{
			$this->login(); 
		}	
	}
	public function libros_agregar(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="bibliotecario")){
			if(POST("registrar")){
				//$uploaddir = 'c:\xampp/htdocs/biblioteca/www/lib/themes/biblioteca/images/';
				
				$uploaddir = '/var/www/biblioteca/www/lib/themes/biblioteca/images/';
				$uploadfile = $uploaddir.basename($_FILES['bookfile']['name']);
				$namefile = basename($_FILES['bookfile']['name']);
				$namefile2 = basename($_FILES['bookfile2']['name']);
				$namefile3 = basename($_FILES['bookfile3']['name']);
				$namefile4 = basename($_FILES['bookfile4']['name']);

				//$logs = $this->biblioteca_Model->logs("Subir Archivo");		    	
              	if (move_uploaded_file($_FILES['bookfile2']['tmp_name'], $uploadfile)) {
						chmod($uploadfile, 0755);
				}
					
				if (move_uploaded_file($_FILES['bookfile3']['tmp_name'], $uploadfile)) {
						chmod($uploadfile, 0755);
				} 

				if (move_uploaded_file($_FILES['bookfile4']['tmp_name'], $uploadfile)) {
						chmod($uploadfile, 0755);
				}

				if (move_uploaded_file($_FILES['bookfile']['tmp_name'], $uploadfile)) {
					chmod($uploadfile, 0755);
	
					$data3=$this->biblioteca_Model->libros_buscar_deway(POST("dewey"));

					if(POST("dewey")=="0"){
						//$data3=$this->biblioteca_Model->libros_buscar_deway(POST("dewey"));
						$dewey_decimal=$data3["0"]["id_dewey_decimal"]+1;
					}elseif (POST("dewey")=="1") {
						//$data3=$this->biblioteca_Model->libros_buscar_deway(POST("dewey"));
						$dewey_decimal=$data3["0"]["id_dewey_decimal"]+1;
					}elseif (POST("dewey")=="2") {
						//$data3=$this->biblioteca_Model->libros_buscar_deway(POST("dewey"));
						$dewey_decimal=$data3["0"]["id_dewey_decimal"]+1;
					}elseif (POST("dewey")=="3") {
						//$data3=$this->biblioteca_Model->libros_buscar_deway(POST("dewey"));
						$dewey_decimal=$data3["0"]["id_dewey_decimal"]+1;
					}elseif (POST("dewey")=="4") {
						//$data3=$this->biblioteca_Model->libros_buscar_deway(POST("dewey"));
						$dewey_decimal=$data3["0"]["id_dewey_decimal"] + 1;
					}elseif (POST("dewey")=="5") {
						//$data3=$this->biblioteca_Model->libros_buscar_deway(POST("dewey"));
						$dewey_decimal=$data3["0"]["id_dewey_decimal"]+1;
					}elseif (POST("dewey")=="6") {
						//$data3=$this->biblioteca_Model->libros_buscar_deway(POST("dewey"));
						$dewey_decimal=$data3["0"]["id_dewey_decimal"]+1;
					}elseif (POST("dewey")=="7") {
						//$data3=$this->biblioteca_Model->libros_buscar_deway(POST("dewey"));
						$dewey_decimal=$data3["0"]["id_dewey_decimal"]+1;
					}elseif (POST("dewey")=="8") {
						//$data3=$this->biblioteca_Model->libros_buscar_deway(POST("dewey"));
						$dewey_decimal=$data3["0"]["id_dewey_decimal"]+1;
					}elseif (POST("dewey")=="9") {
						//$data3=$this->biblioteca_Model->libros_buscar_deway(POST("dewey"));
						$dewey_decimal=$data3["0"]["id_dewey_decimal"]+1;
					}
				
					$data = $this->biblioteca_Model->libro_agregar($namefile,$namefile2,$namefile3,$namefile4,$dewey_decimal);
              		echo "<script>alert('Se ha guardado Correctamente');</script>";
					$this->home();
				} else {
					echo "<script>alert('Error en la carga del archivo'".basename($_FILES['bookfile']['name'])."')</script>";
					$this->libros_agregar();
				}

			}else{
			
				$data = $this->biblioteca_Model->dewey_todos();
				$vars["dewey"] = $data;

				$this->title("Crear Libro");
				$vars["view"] = $this->view("libros/agregar",TRUE);
				$this->render("content",$vars);
			}
		}else{
			$this->login();
		}	
	}
	public function libros_busqueda(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="bibliotecario")){
			$this->libros_todos();
		}else{
			$this->login();
		}
	}
	public function libros_prestar(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="bibliotecario")){
			if(POST("registrar")){
				$data1 = $this->biblioteca_Model->libro_buscar(POST("libro_prestar"));
				$cantidad = $data1[0]["cantidad"];
				if(($cantidad=="0")||($cantidad=="1")){
					echo "<script>alert('No se puede prestar este libro.')</script>";
					$this->home();
				}else{
					$this->biblioteca_Model->libro_prestar(POST("libro_prestar"),POST("persona_prestar"),$cantidad);
					echo "<script>alert('Se ha guardado Correctamente');</script>";
					$this->home();
				}
			}else{
				$data = $this->biblioteca_Model->libros_todos();
				$vars["libros"] = $data;
				$vars["usuarios"]  = $this->biblioteca_Model->buscar_todos_usuarios_users();

				$this->title("Prestar Libro");

				$vars["view"] = $this->view("libros/prestar",TRUE);
				$this->render("content",$vars);
			}
		}else{
			$this->login();
		}	
	}
	public function libros_reservar(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="bibliotecario")){
			if(POST("registrar")){
				$data1 = $this->biblioteca_Model->libro_buscar(POST("libro_prestar"));
				$cantidad = $data1[0]["cantidad"];
				if(($cantidad=="0")||($cantidad=="1")){
					$this->biblioteca_Model->libro_reservar(POST("libro_prestar"),POST("persona_prestar"),$cantidad);
					echo "<script>alert('No se puede prestar este libro pero en lo que se pueda te informaremos')</script>";
					$this->home();
				}else{
					$this->biblioteca_Model->libro_reservar(POST("libro_prestar"),POST("persona_prestar"),$cantidad);
					echo "<script>alert('Puedas pasar buscando el libro');</script>";
					$this->home();
				}
			}else{
				$data = $this->biblioteca_Model->libros_todos();
				$vars["libros"] = $data;
				$vars["usuarios"]  = $this->biblioteca_Model->buscar_todos_usuarios();

				$this->title("Prestar Libro");

				$vars["view"] = $this->view("libros/prestar",TRUE);
				$this->render("content",$vars);
			}
		}else{
			$this->login();
		}	
	}
	public function libro_update_estado(){
		$id_prestamo=$_GET["prestamo"];
		$this->biblioteca_Model->libro_update_estado($id_prestamo);
		$this->home();
	}
	public function libros_buscar() {
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="bibliotecario")){
			$data = $this->biblioteca_Model->libros_buscar_espesifico(POST("buscar"));
			//____($data);
			if($data == false){
				echo "<script>alert('No se encontraron Libros');</script>";
				$this->home();
			}else{
				$vars["libros"] = $data;
				$vars["view"] = $this->view("libros/buscar_libro",TRUE);
				$this->render("content",$vars);
				
				$logs = $this->biblioteca_Model->logs("Buscar Libros");
					
			}
		}else{
			$this->login(); 
		}
	}

	public function libros_todos() {
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="bibliotecario")){
			$data = $this->biblioteca_Model->libros_todos();
			$data1 = $this->biblioteca_Model->dewey_todos();
			//____($data);
			if($data == false){
				echo "<script>alert('No Hay Libros Guardados');</script>";
				$this->libros_agregar();
			}else{
				$this->title("Todos los libros");
				$vars["libros"] = $data;
				$vars["dewey"] = $data1;
				$vars["view"] = $this->view("libros/todos",TRUE);
				$this->render("content",$vars);
				
				$logs = $this->biblioteca_Model->logs("Ver Todos");
			}
		}else{
			$this->login(); 
		}
	}
	public function libros_perfil(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="bibliotecario")){
			$data = $this->biblioteca_Model->libro_buscar(POST("libro_id"));	
			$data2 = $this->biblioteca_Model->dewey_todos();	
			$vars["info_libro"] = $data;
			$vars["deway"] = $data2;
			$vars["view"] = $this->view("libros/perfil",TRUE);
			$this->render("content",$vars);
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}
	public function libro_datos(){
		$data = $this->biblioteca_Model->libro_buscar($_GET["id"]);	
		$vars["mision"]  = $this->biblioteca_Model->informacion_status('Mision');
		$vars["vision"]  = $this->biblioteca_Model->informacion_status('Vision');
		$vars["quienes_somos"]  = $this->biblioteca_Model->datos_ubicacion();
		$vars["informacion_ubicacion"]  = $this->biblioteca_Model->datos_ubicacion();
		$vars["dewey"]  = $this->biblioteca_Model->dewey_todos();
		$vars["info_libros"] = $data;
		$vars["view"] = $this->view("frontend/libro_datos",TRUE);
		$this->render("content",$vars);
		
	}
	public function enviar_mensaje_interno() {
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			$this->home();
		}else{
			if(POST("enviar_btn")){
				$data = $this->biblioteca_Model->enviar_mensaje_interno();
				//____($data);
				if($data==FALSE){
					echo "<script>alert('Tu mensaje no fue enviado, Intenta mas tarde')</script>";
					$this->contactanos();
				}else{
					echo "<script>alert('Tu mensaje fue enviado, en unas horas te contactaremos')</script>";
					$this->frontend();
				}

				
			}else{ }
		}
	}
	public function listas_mensajes() {
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="bibliotecario")){
			$data = $this->biblioteca_Model->buscar_todos_mensajes();
			//____($data);
			if($data == false){
				echo "<script>alert('no hay mensajes en el sistema'); document.location.href='';</script>";
			}else{
				$this->title("Lista Mensajes");
				$vars["mensajes_interno"] = $data;
				$vars["view"] = $this->view("listas_mensajes",TRUE);
				$this->render("content",$vars);
				//$logs = $this->panel_Model->logs("Ver Listas de Usuarios");
			}
		}else{
			$this->login(); 
		}
	}
	

	public function buscar_dewey(){
		$data = $this->biblioteca_Model->libros_buscar_dewey($_GET["id"]);	
		$vars["publicidad"]  = $this->biblioteca_Model->informacion_status('Publicidad');
		$vars["mision"]  = $this->biblioteca_Model->informacion_status('Mision');
		$vars["vision"]  = $this->biblioteca_Model->informacion_status('Vision');
		$vars["ultimos_libros"]  = $data;
		$vars["dewey"]  = $this->biblioteca_Model->dewey_todos();
		$vars["quienes_somos"]  = $this->biblioteca_Model->datos_ubicacion();
		$vars["informacion_ubicacion"]  = $this->biblioteca_Model->datos_ubicacion();
		$vars["view"] = $this->view("frontend/catalogo",TRUE);
		$this->render("content",$vars);
	}
	public function libros_modificar(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="bibliotecario")){
			if(POST("modificar")){
				$data = $this->biblioteca_Model->libros_modificar(POST("libro_id"));
				$this->libros_todos();
			}
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}
	public function libros_borrar(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="bibliotecario")){
			if(POST("borrar")){
				$data = $this->biblioteca_Model->libros_borrar(POST("libro_id"));
				$this->libros_todos();
			}
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}
	public function libros_prestados_todos(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="bibliotecario")){
			$data = $this->biblioteca_Model->libros_todos();
			$data1 = $this->biblioteca_Model->libros_prestados_todos();
			//____($data);
			if($data == false){
				echo "<script>alert('No Hay Libros Prestados a personas');</script>";
				$this->libros_agregar();
			}else{
				$this->title("Todos los libros");
				$vars["libros"] = $data;
				$vars["prestamos"] = $data1;
				$vars["view"] = $this->view("libros/prestados_libros",TRUE);
				$this->render("content",$vars);
				
				$logs = $this->biblioteca_Model->logs("Ver Libros Prestados");
			}
		}else{
			$this->login(); 
		}		
	}
	public function libros_prestados_usuario(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="bibliotecario")){
			$data = $this->biblioteca_Model->libros_todos();
			$data1 = $this->biblioteca_Model->libros_prestados_usuario(SESSION('id'));
			//____($data);
			if($data == false){
				echo "<script>alert('No Hay Libros Prestados');</script>";
				$this->libros_agregar();
			}else{
				$this->title("Todos los libros prestados");
				$vars["libros"] = $data;
				$vars["prestamos"] = $data1;
				$vars["view"] = $this->view("libros/prestados_libros",TRUE);
				$this->render("content",$vars);
				
				$logs = $this->biblioteca_Model->logs("Ver Libros Prestados");
			}
		}else{
			$this->login(); 
		}		
	}
	public function libros_prestados_usuario_prestados(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="bibliotecario")){
			$data = $this->biblioteca_Model->libros_todos();
			$data1 = $this->biblioteca_Model->libros_prestados_usuario_prestados(SESSION('id'),"Prestado");
			//____($data);
			if($data == false){
				echo "<script>alert('No Hay Libros Prestados');</script>";
				$this->libros_agregar();
			}else{
				$this->title("Todos los libros prestados");
				$vars["libros"] = $data;
				$vars["prestamos"] = $data1;
				$vars["view"] = $this->view("libros/prestados_libros",TRUE);
				$this->render("content",$vars);
				
				$logs = $this->biblioteca_Model->logs("Ver Libros Prestados");
			}
		}else{
			$this->login(); 
		}		
	}
	public function libros_prestados_usuario_entregados(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="bibliotecario")){
			$data = $this->biblioteca_Model->libros_todos();
			$data1 = $this->biblioteca_Model->libros_prestados_usuario_prestados(SESSION('id'),"Entregado");
			//____($data);
			if($data == false){
				echo "<script>alert('No Hay Libros Prestados');</script>";
				$this->libros_agregar();
			}else{
				$this->title("Todos los libros prestados");
				$vars["libros"] = $data;
				$vars["prestamos"] = $data1;
				$vars["view"] = $this->view("libros/prestados_libros",TRUE);
				$this->render("content",$vars);
				
				$logs = $this->biblioteca_Model->logs("Ver Libros Prestados");
			}
		}else{
			$this->login(); 
		}		
	}
	public function libros_prestados_personas(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="bibliotecario")){
			$data = $this->biblioteca_Model->buscar_todos_usuarios();
			$data1 = $this->biblioteca_Model->libros_prestados_status("Prestado","Reservado");
			//____($data);
			if($data == false){
				echo "<script>alert('No Hay Libros Prestados a personas');</script>";
				$this->libros_agregar();
			}else{
				$this->title("Todos los libros");
				$vars["libros"] = $data;
				$vars["prestamos"] = $data1;
				$vars["view"] = $this->view("libros/prestados_personas",TRUE);
				$this->render("content",$vars);
				
				$logs = $this->biblioteca_Model->logs("Ver Libros Prestados");
			}
		}else{
			$this->login(); 
		}		
	}
	public function libros_prestados(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="bibliotecario")){
			$data = $this->biblioteca_Model->libros_todos();
			$data1 = $this->biblioteca_Model->libros_prestados_status("Prestado","");
			//____($data);
			if($data == false){
				echo "<script>alert('No Hay Libros Prestados a personas');</script>";
				$this->libros_agregar();
			}else{
				$this->title("Todos los libros");
				$vars["libros"] = $data;
				$vars["prestamos"] = $data1;
				$vars["view"] = $this->view("libros/prestados_libros",TRUE);
				$this->render("content",$vars);
				
				$logs = $this->biblioteca_Model->logs("Ver Libros Prestados");
			}
		}else{
			$this->login(); 
		}
	}
	public function libros_entregar(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="bibliotecario")){
			if(POST("entregar")){
				$data = $this->biblioteca_Model->libro_buscar(POST("libro_id"));
				$cantidad = $data[0]["cantidad"];
				$this->biblioteca_Model->libro_entregar(POST("prestamo_id"),POST("libro_id"),$cantidad);
				
				echo "<script>alert('Se ha guardado Correctamente');</script>";
				$this->home();
			}
		}else{
			$this->login();
		}
	}
	public function respaldar_bd(){
		if(SESSION("tipo_user")=="admin"){
			echo "<script> document.location.href='http://localhost/biblioteca/www/applications/biblioteca/controllers/backupdb.php'; alert('se ha guardado con exito')</script>";
			$logs = $this->biblioteca_Model->logs("Respaldar BD");
			$this->home();
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}

	public function respaldar_files(){
		if(SESSION("tipo_user")=="admin"){
			exec("tar -zcvf biblioteca_files.tar /var/www/biblioteca/www/lib/files/ ");
			echo "<script>alert('El respaldo comprimido de los ficheros se encuenta en la carpeta raiz del sistema');</script>";
			$logs = $this->biblioteca_Model->logs("Respaldar Ficheros");
			echo $this->home();
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}

	public function reporte_usuarios_global(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="bibliotecario")){	
			echo "<script> document.location.href='".get("webURL")."/www/classes/reporte_usuarios.php';</script>";
			$logs = $this->biblioteca_Model->logs("Reporte Usuarios");
			$this->home();
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}	
	}
	public function reporte_archivos_global(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="bibliotecario")){
			echo "<script> document.location.href='".get("webURL")."/www/classes/reporte_libros.php';</script>";
			$logs = $this->biblioteca_Model->logs("Reporte Archivos");
			$this->home();
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}
	public function reporte_libro_clasificacion(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="bibliotecario")){
			if(POST("buscar")){
				$_GET['dewey']=POST('dewey');
				echo "<script> document.location.href='".get("webURL")."/www/classes/reporte_libros_clasificacion.php?dewey=".$_GET['dewey']."';</script>";
				$logs = $this->biblioteca_Model->logs("Reporte Archivos");
				$this->home();
			}else{
				$this->title("reporte libro clasificacion");
				$vars["dewey"] = $this->biblioteca_Model->dewey_todos();
				$vars["view"] = $this->view("reporte",TRUE);
				$this->render("content",$vars);
			}
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}
	public function reporte_logs_global(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="bibliotecario")){
			echo "<script> document.location.href='".get("webURL")."/www/classes/reporte_logs.php';</script>";
			$logs = $this->biblioteca_Model->logs("Reporte Logs");
			$this->home();
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}
/*****/
	public function plantillas_agregar(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="bibliotecario")){
			if(POST("registrar")){
				$this->biblioteca_Model->plantilla_agregar();
				//____($data);
				echo "<script>alert('Se ha guardado Correctamente');</script>";
				$this->home();
			}else{
				$this->title("Crear Plantilla");
				$vars["view"] = $this->view("plantillas/agregar",TRUE);
				$this->render("content",$vars);
			}
		}else{
			$this->login();
		}	
	}
	public function plantillas_buscar() {
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="bibliotecario")){
			$data = $this->biblioteca_Model->archivos_buscar(POST("buscar"));
			//____($data);
			if($data == false){
				echo "<script>alert('No se encontraron Documentos');</script>";
				$this->home();
			}else{
				$vars["archivos"] = $data;
				$vars["view"] = $this->view("archivo/buscar_archivo",TRUE);
				$this->render("content",$vars);
				
				$logs = $this->biblioteca_Model->logs("Buscar Archivos");
					
			}
		}else{
			$this->login(); 
		}
	}

	public function plantillas_todos() {
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="bibliotecario")){
			$data = $this->biblioteca_Model->plantilla_todos();
			if($data == false){
				echo "<script>alert('No Hay Plantillas Guardados');</script>";
				$this->plantillas_agregar();
			}else{
				$this->title("Todos las Plantillas");
				$vars["Plantillas"] = $data;
				$vars["view"] = $this->view("plantillas/todos",TRUE);
				$this->render("content",$vars);
				
				$logs = $this->biblioteca_Model->logs("Ver Todas plantillas");
			}
		}else{
			$this->login(); 
		}
	}
	
	public function plantillas_modificar(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="bibliotecario")){
			if(POST("modificar")){
				$data = $this->biblioteca_Model->plantilla_modificar(POST("plantilla_id"));
				$this->plantillas_todos();
			}
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}
	public function plantillas_borrar(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="bibliotecario")){
			if(POST("borrar")){
				$data = $this->biblioteca_Model->plantilla_borrar(POST("plantilla_id"));
				$this->plantillas_todos();
			}
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}
}
