<?php
/**
 * Access from index.php:
 */
if(!defined("_access")) {
	die("Error: You don't have permission to access here...");
}

class biblioteca_Model extends ZP_Model {
	
	public function __construct() {
		$this->Db = $this->db();		
		$this->Data = $this->core("Data");
		$this->helpers();
		$this->table_users = "users";
		$this->table_libros = "libros";
		$this->table_config = "configuracion";
		$this->table_prestamos = "prestamos";
		$this->table_dewey = "dewey";
		$this->table_mensajes = "mensajes_internos";
	}
	//Funciones generales del sistema
	public function datos_ubicacion(){
		$this->Db->table($this->table_config);
		return $data = $this->Db->findBySQL("id='1'");	
	}
	public function buscar_todos_informacion(){
		return $todos_usuarios = $this->Db->findAll("informacion");
	}
	public function informacion_agregar(){
		 $data = array(
			"nombre"=>POST("name"),
			"descripcion"=>POST("descripcion"),
			"tipo"=>POST("info_tipo"),
			"autor"=>SESSION('id'),
			"fecha"=>date("d-m-Y")
		);
		$this->Db->insert("informacion", $data);
	}
	public function info_modificar($info_id){
		$data = array(
		 	"nombre"=>POST("name"),
		 	"descripcion"=>POST("descripcion"),
		 	"tipo"=>POST("tipo")
		);
		$this->Db->update("informacion", $data, $info_id);
	}
	public function info_borrar($id){
		$this->Db->delete($id, "informacion");
	}
	public function login(){
		$_session["usuario"] = $username = POST("usuario");
		$_session["clave"] = $password = POST("clave");
		$this->Db->table($this->table_users);
		return $data = $this->Db->findBySQL("usuario='$username' AND clave= '$password'");
	}
	
	//funciones de usuarios
	public function registrar(){
		 $data = array(
		 	"nacionalidad"=>POST("nacionalidad"),
		 	"ci"=>POST("ci"),
		 	"nombre"=>POST("nombres"),
		 	"apellido"=>POST("apellidos"),
		 	"usuario"=>POST("usuario"),
		 	"clave"=>POST("clave"),
			"fecha_nac"=>POST("fecha"),
			"telefono"=>POST("telefono"),
			"tipo_user"=>"users",
			"estado"=>"activo",
			"correo"=>POST("email"),
			"pregunta"=>POST("pregunta"),
			"respuesta"=>POST("respuesta"),
			"fecha_registro"=>date("d-m-Y")
		);
		$this->Db->insert("users", $data);
	}
	public function modificar_perfil($users_id,$namefile){
		$data = array(
		 	"ci"=>POST("ci"),
		 	"ruta_img"=>$namefile,
		 	"nombre"=>POST("nombres"),
		 	"apellido"=>POST("apellidos"),
		 	"telefono"=>POST("telefono"),
		 	"fecha_nac"=>POST("fecha"),
			"correo"=>POST("email")
		);
		$this->Db->update("users", $data, $users_id);
	}
	public function modificar_clave($user_id){
		$data = array(
		 	"clave"=>POST("clave")
		);
		$this->Db->update("users", $data, $user_id);
	}
	public function buscar_pregunta_usuario($correo){
		$this->Db->table($this->table_users);
		return $data = $this->Db->findBySQL("correo='$correo'");
	}
	public function usuario_borrar($id){
		 $this->Db->delete($id, $this->table_users);
	}
	public function usuario_estado($id,$status){
		$data = array(
		 	"estado"=>$status
		);
		$this->Db->update("users", $data, $id);
	}
	public function buscar_todos_usuarios(){
		return $todos_usuarios = $this->Db->findAll($this->table_users);
	}
	public function buscar_todos_usuarios_users(){
		$this->Db->table($this->table_users);
		return $data = $this->Db->findBySQL("tipo_user='users'");
	}
	public function logs($accion){
		 $data = array(
		 	"ci_user"=>SESSION("ci"),
		 	"accion"=>$accion,
		 	"fecha_registro"=>date("d-m-Y")
		);
		$this->Db->insert("logs", $data);
	}

	public function buscar_logs($ci_user){
		$this->Db->table($this->table_logs);
		return $data = $this->Db->findBySQL("ci_user='$ci_user' ORDER BY id  DESC LIMIT 10 ");
	}
	//funcionesPOST("cnt_libros"),
	public function dewey_todos(){
		return $data = $this->Db->findAll("dewey");
	}
	public function libros_todos(){
		return $data = $this->Db->findAll("libros");
	}
	public function libro_agregar($namefile,$namefile2,$namefile3,$namefile4,$dewey_decimal){
		$data = array(
		 	"id_dewey"=>POST("dewey"),
		 	"id_dewey_decimal"=>$dewey_decimal,
		 	"nombre"=>POST("nombre_libro"),
		 	"descripcion"=>POST("descripcion"),
		 	"tags"=>POST("tags"),
		 	"autor"=>POST("autor"),
		 	"cantidad" => POST("cnt_libros"),
		 	"img_ruta"=>$namefile,
		 	"img_ruta2"=>$namefile2,
		 	"img_ruta3"=>$namefile3,
		 	"img_ruta4"=>$namefile4,
		 	"fecha_registro"=>date("d-m-Y")
		);
		$this->Db->insert("libros", $data);	
	}
	
	public function libros_buscar_deway($id_dewey){
		$this->Db->table($this->table_libros);
		return $data = $this->Db->findBySQL("id_dewey='$id_dewey' ORDER BY id DESC limit 1");
	}
	public function libros_buscar_dewey($id_dewey){
		$this->Db->table($this->table_libros);
		return $data = $this->Db->findBySQL("id_dewey='$id_dewey'");
	}
	public function libro_prestar($id_libro,$id_user,$cantidad){
		$data = array(
		 	"id_user"=>$id_user,
		 	"id_libro"=>$id_libro,
		 	"id_bibliotecario"=>SESSION("id"),
		 	"status"=>"Prestado",
		 	"cant_dias_prestamo"=>POST("cant_dias"),
		 	"fecha_prestamo_retorno"=>POST("fecha_retorno"),
		 	"fecha_registro"=>date("d-m-Y")
		);
		$this->Db->insert("prestamos", $data);
		
		$data1 = array(
		 	"cantidad"=>$cantidad-1
		);
		$this->Db->update("libros", $data1, $id_libro);	
	}
	public function libro_reservar($id_libro,$id_user,$cantidad){
		$data = array(
		 	"id_user"=>$id_user,
		 	"id_libro"=>$id_libro,
		 	"id_bibliotecario"=>SESSION("id"),
		 	"status"=>"Reservado",
		 	"cant_dias_prestamo"=>POST("cant_dias"),
		 	"fecha_prestamo_retorno"=>POST("fecha_retorno"),
		 	"fecha_registro"=>date("d-m-Y")
		);
		$this->Db->insert("prestamos", $data);
		
		$data1 = array(
		 	"cantidad"=>$cantidad-1
		);
		$this->Db->update("libros", $data1, $id_libro);	
	}
	public function libro_update_estado($id_prestamo){
		$data1 = array(
		 	"status"=>"Prestado"
		);
		return $this->Db->update("prestamos", $data1, $id_prestamo);	
	}
	public function libro_entregar($id_prestamo,$id_libro,$cantidad){
		$data = array(
		 	"status"=>"Entregado"
		);
		$this->Db->update("prestamos", $data, $id_prestamo);	

		$data1 = array(
		 	"cantidad"=>$cantidad+1
		);
		$this->Db->update("libros", $data1, $id_libro);	
	}

    public function libros_prestados_todos(){
		return $data = $this->Db->findAll("prestamos");
	}
	public function libros_prestados_status($status,$status2){
		$this->Db->table($this->table_prestamos);
		return $data = $this->Db->findBySQL("status='$status' OR status='$status2'");
	}
	public function libros_prestados_usuario($id_user){
		$this->Db->table($this->table_prestamos);
		return $data = $this->Db->findBySQL("id_user='$id_user'");
	}
	public function libros_prestados_usuario_prestados($id_user,$status){
		$this->Db->table($this->table_prestamos);
		return $data = $this->Db->findBySQL("id_user='$id_user' AND status='$status'");
	}
	public function libros_modificar($libro_id){
		$data = array(
		 	"nombre"=>POST("name"),
		 	"descripcion"=>POST("descripcion"),
		 	"tags"=>POST("tags")
		);
		$this->Db->update("libros", $data, $libro_id);
	}
	public function libros_borrar($id){
		$this->Db->delete($id, "libros");
	}
	public function archivos_guardar_status($archivo_id,$status){
		$data = array(
		 	"estado"=>$status
		);
		$this->Db->update("archivos", $data, $archivo_id);
	}
	public function libro_buscar($libro_id){
		$this->Db->table($this->table_libros);
		return $data = $this->Db->findBySQL("id='$libro_id'");	
	}
	public function libros_ultimos($limit){
		return $data = $this->Db->Query("SELECT * FROM sv_libros ORDER BY id  DESC LIMIT $limit");
	}

	/*funciones de planillas */
	public function plantilla_todos(){
		return $data = $this->Db->findAll("plantillas");
	}
	public function plantilla_agregar(){
		$data = array(
		 	"id_user"=>SESSION("id"),
		 	"nombre"=>POST("nombre_plantilla"),
		 	"descripcion"=>POST("descripcion"),
		 	"tipo"=>POST("tipo"),
		 	"fecha_registro"=>date("d-m-Y")
		);
		$this->Db->insert("plantillas", $data);	
	}
	public function plantilla_modificar($plantilla_id){
		$data = array(
		 	"nombre"=>POST("name"),
		 	"descripcion"=>POST("descripcion")
		);
		$this->Db->update("plantillas", $data, $plantilla_id);
	}
	public function plantilla_borrar($id){
		$this->Db->delete($id, "plantillas");
	}
	public function informacion_status($tipo){
		return $data = $this->Db->Query("SELECT * FROM sv_informacion WHERE tipo='$tipo' ORDER BY id  ASC LIMIT 4");
	}
	public function libros_buscar_por($buscar){
		return $data = $this->Db->Query("SELECT * FROM sv_libros WHERE nombre like '$buscar%' ORDER BY nombre  DESC");
	}
	public function libros_buscar_espesifico($buscar){
		return $data = $this->Db->Query("SELECT * FROM sv_libros WHERE nombre like '%$buscar%' OR autor like '%$buscar%' OR descripcion like '%$buscar%' OR id_dewey like '%$buscar%' ORDER BY nombre  DESC");
	}
	public function enviar_mensaje_interno(){
		$data = array(
		 	"nombres"=>POST("nombres"),
		 	"email"=>POST("email"),
		 	"telefono"=>POST("telefono"),
		 	"estado"=>POST("estado"),
		 	"mensaje"=>POST("mensaje"),
		 	"fecha"=>date("d-m-Y")
		);
		return $this->Db->insert("mensajes_internos", $data);
	}
	public function buscar_todos_mensajes(){
		return $toda_informacion = $this->Db->findAll($this->table_mensajes);
	}
}
