CREATE DATABASE IF NOT EXISTS biblioteca;

USE biblioteca;

DROP TABLE IF EXISTS sv_configuracion;

CREATE TABLE `sv_configuracion` (
  `id` int(255) NOT NULL auto_increment,
  `nombre` varchar(100) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `telefono` varchar(14) NOT NULL,
  ` correo` varchar(150) NOT NULL,
  `facebook` varchar(150) NOT NULL,
  `twitter` varchar(150) NOT NULL,
  `url_logo` varchar(150) NOT NULL,
  `url_fondo` varchar(200) NOT NULL,
  `autor` int(255) NOT NULL,
  `fecha_registro` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO sv_configuracion VALUES("1","Biblioteca Publica Andrés Eloy Blanco","Barinas Estado Barinas  Centro de Barinas con Avenida Brinceño Méndez cruce con Calle Carvaja","0273-215-09-02","bibliotecapublica-aeb@hotmail.com","","@BPAndresEloyBlanco","http://localhost/biblioteca/www/lib/themes/biblioteca/img/logo.png","","0","19-07-2014");



DROP TABLE IF EXISTS sv_dewey;

CREATE TABLE `sv_dewey` (
  `id` int(255) NOT NULL auto_increment,
  `codigo` varchar(3) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `descripcion` text NOT NULL,
  `autor` int(255) NOT NULL,
  `fecha_registro` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

INSERT INTO sv_dewey VALUES("1","00","Obras Generales","Obras Generales","0","19-07-2014");
INSERT INTO sv_dewey VALUES("2","100","Filosofía y Psicología","Filosofía y Psicología","0","19-07-2014");
INSERT INTO sv_dewey VALUES("3","200","Religión","Religión","0","19-07-2014");
INSERT INTO sv_dewey VALUES("4","300","Ciencias Sociales","Ciencias Sociales (Sociología,  Política, Economía, Derecho, Premilitar, Educación, Folklore, Ambiente)","0","19-07-2014");
INSERT INTO sv_dewey VALUES("5","400","Lingüística","Lingüística (Ingles)","0","19-07-2014");
INSERT INTO sv_dewey VALUES("6","500","Ciencias Puras","Ciencias Puras (Matemática, Física, Química, Geología, Ciencia de la Tierra, Biología, Botánica, Salud, Dibujo Técnico, Medicina, Agricultura)","0","19-07-2014");
INSERT INTO sv_dewey VALUES("7","600","Ciencia Aplicada","Ciencia Aplicada (Administración)","0","19-07-2014");
INSERT INTO sv_dewey VALUES("8","700","Arte y Recreación","Arte y Recreación","0","19-07-2014");
INSERT INTO sv_dewey VALUES("9","800","Literatura","Literatura (Novelas, Novelas de Venezuela,  Poesía, Poesía de Venezuela, Teatro, Teatro de Venezuela)","0","19-07-2014");
INSERT INTO sv_dewey VALUES("10","900","Geografía","Geografía (Historia, Educación Artística, Geografía General, Geografía de Venezuela, Catedra de Bolivariana, Historia de Venezuela, Historia Universal)","0","19-07-2014");



DROP TABLE IF EXISTS sv_informacion;

CREATE TABLE `sv_informacion` (
  `id` int(255) NOT NULL auto_increment,
  `nombre` varchar(255) NOT NULL,
  `descripcion` text NOT NULL,
  `tipo` varchar(20) NOT NULL,
  `autor` int(255) NOT NULL,
  `fecha` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

INSERT INTO sv_informacion VALUES("1","Historia","Barinas forma parte de la red de bibliotecas públicas desde 1977, para el mes de mayo se crea la red de bibliotecas públicas a través de decreto, y se funda la biblioteca pública “Andrés Eloy Blanco” y es puesta en funcionamiento para el público en general, en Enero de 1978.  Andrés Eloy Blanco es una institución pública perteneciente a la red de bibliotecas públicas del país, adscrita a la Secretaria Ejecutiva De Educación, bajo los lineamientos funcionales de biblioteca nacional, con el objetivo de seleccionar y organizar los textos bibliográficos que a ella llegan (Compra- Donación); con el fin de ponerlos a disposición del público en general, para que, todos los sectores tengan acceso a ella, y así cumplir con un derecho ciudadano que el estado garantiza.","Publicidad","1","29-05-2014");
INSERT INTO sv_informacion VALUES("2","¿Para que Sirve?","Mediante este servicio, la Biblioteca te permite disponer de sus fondos bibliográficos, para su uso fuera de sus instalaciones por un periodo de tiempo determinado. Los usuarios podrán acceder a este servicio registrandose en la pagina y asi entregarles su carne que les acredite como tales","Publicidad","1","29-05-2014");
INSERT INTO sv_informacion VALUES("3","¿Quien tiene Acceso?","\n         Todo los usuarios con carné de la Biblioteca actualizado","publicidad","1","");
INSERT INTO sv_informacion VALUES("4","Condiciones del Prestamo","Los prestamos tendran un limite de 3 Dias sin execcion y solo si existen mas de 3 ejemplares\n  Si el libro de tu interes no esta disponible puedes reservarlo y cuando haya la disponibilidad se le avisará a través de mensaje de texto o correo electronico que te hayas registrado comprueba en los mostradores de información y préstamo que tenemos tu e-mail en nuestra base de datos","publicidad","1","29-05-2014");
INSERT INTO sv_informacion VALUES("5","Penalizacion","Una vez vencido el plazo de devolución, si la obra no ha sido devuelta, la autorización para nuevos préstamos queda suspendida. Se informará al usuario mediante un correo electrónico o mensaje de texto.\nLa penalización por la devolución de obras con retraso es de 1 mes de suspensión de carnet de prestamos \nSi la obra no ha sido devuelta después de tres avisos de reclamación, se cancelará el acceso del usuario a la Biblioteca hasta la devolución de la obra en préstamo.\nLa pérdida o deterioro de un libro supone la desautorización de nuevos préstamos hasta la reposición del mismo o, en su defecto, el abono de su importe.\nEl carné es personal e intransferible, por lo que su utilización por otra persona será sancionado.","publicidad","1","29-05-2014");
INSERT INTO sv_informacion VALUES("7","Vision ","la vision es X Y Z","Vision","1","29-09-2014");
INSERT INTO sv_informacion VALUES("9","Mision","la mision va aca y tiene 2000 caracteres para llenar","Mision","1","13-10-2014");



DROP TABLE IF EXISTS sv_libros;

CREATE TABLE `sv_libros` (
  `id` int(255) NOT NULL auto_increment,
  `id_dewey` int(255) NOT NULL,
  `img_ruta` varchar(100) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `tags` varchar(255) NOT NULL,
  `autor` varchar(255) NOT NULL,
  `cantidad` int(2) NOT NULL,
  `usuario_id` int(255) NOT NULL,
  `fecha_registro` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `id_dewey` (`id_dewey`),
  CONSTRAINT `sv_libros_ibfk_1` FOREIGN KEY (`id_dewey`) REFERENCES `sv_dewey` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO sv_libros VALUES("1","7","conf_sistema.png","aventuras","aventuras","aventuras","aventuras","1","1","29-09-2014");
INSERT INTO sv_libros VALUES("2","9","Captura de pantalla de 2014-05-17 19:15:28.png","juego de tronos","batallas epicas y muertes que jode","juego, tronos, guerra, otros","RR martin","6","1","29-09-2014");
INSERT INTO sv_libros VALUES("3","9","Captura de pantalla de 2014-03-16 03:56:04.png","cancion de fuego y hielo","batallas epicas y muertes que jode","asdasdas","RR martin","8","1","29-09-2014");
INSERT INTO sv_libros VALUES("4","9","Captura de pantalla de 2014-02-13 01:58:49.png","choque de reyes","batallas epicas y muertes que jode","ASDASDAS","RR martin","6","1","29-09-2014");
INSERT INTO sv_libros VALUES("5","9","Captura de pantalla de 2014-03-16 03:56:04.png","danza de dragones","batallas epicas y muertes que jode","asdas","RR martin","9","1","29-09-2014");
INSERT INTO sv_libros VALUES("6","9","Captura de pantalla de 2014-03-16 03:56:04.png","cancion de fuego y hielo","batallas epicas","batallas, novela, rrmartin","RR Martin","5","1","29-09-2014");
INSERT INTO sv_libros VALUES("7","7","conf_sistema.png","Ajax","JS Ajax libros","libro, ajax","1","9","1","11-10-2014");
INSERT INTO sv_libros VALUES("8","1","Captura de pantalla de 2014-02-13 01:58:40.png","libro prueba","libro de presentación para prueba","ejemplo, ninguna, no se","0","1","0","14-10-2014");



DROP TABLE IF EXISTS sv_logs;

CREATE TABLE `sv_logs` (
  `id` int(255) NOT NULL auto_increment,
  `ci_user` varchar(22) NOT NULL,
  `accion` varchar(35) NOT NULL,
  `fecha_registro` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=698 DEFAULT CHARSET=utf8;

INSERT INTO sv_logs VALUES("1","","Salir","29-09-2014");
INSERT INTO sv_logs VALUES("2","","Entrar","29-09-2014");
INSERT INTO sv_logs VALUES("3","","Reporte Logs","29-09-2014");
INSERT INTO sv_logs VALUES("4","","Salir","29-09-2014");
INSERT INTO sv_logs VALUES("5","00.000.012","Entrar","29-09-2014");
INSERT INTO sv_logs VALUES("6","00.000.012","Salir","29-09-2014");
INSERT INTO sv_logs VALUES("7","00.000.012","Entrar","29-09-2014");
INSERT INTO sv_logs VALUES("8","00.000.012","Entrar","29-09-2014");
INSERT INTO sv_logs VALUES("9","00.000.012","Salir","29-09-2014");
INSERT INTO sv_logs VALUES("10","00.000.012","Entrar","29-09-2014");
INSERT INTO sv_logs VALUES("11","00.000.012","Ver Todos","29-09-2014");
INSERT INTO sv_logs VALUES("12","00.000.012","Ver Todos","29-09-2014");
INSERT INTO sv_logs VALUES("13","00.000.012","Salir","29-09-2014");
INSERT INTO sv_logs VALUES("14","00.000.012","Entrar","30-09-2014");
INSERT INTO sv_logs VALUES("15","","Salir","30-09-2014");
INSERT INTO sv_logs VALUES("16","00.000.012","Entrar","30-09-2014");
INSERT INTO sv_logs VALUES("17","00.000.012","Agregar plantillas","30-09-2014");
INSERT INTO sv_logs VALUES("18","00.000.012","Entrar","01-10-2014");
INSERT INTO sv_logs VALUES("19","00.000.012","Agregar plantillas","01-10-2014");
INSERT INTO sv_logs VALUES("20","00.000.012","Agregar plantillas","01-10-2014");
INSERT INTO sv_logs VALUES("21","00.000.012","Agregar plantillas","01-10-2014");
INSERT INTO sv_logs VALUES("22","00.000.012","Ver Listas de Informacion","01-10-2014");
INSERT INTO sv_logs VALUES("23","00.000.012","Agregar plantillas","01-10-2014");
INSERT INTO sv_logs VALUES("24","00.000.012","Ver plantillas","01-10-2014");
INSERT INTO sv_logs VALUES("25","00.000.012","Ver plantillas","01-10-2014");
INSERT INTO sv_logs VALUES("26","00.000.012","Entrar","01-10-2014");
INSERT INTO sv_logs VALUES("27","00.000.012","Ver plantillas","01-10-2014");
INSERT INTO sv_logs VALUES("28","00.000.012","Ver plantillas","01-10-2014");
INSERT INTO sv_logs VALUES("29","00.000.012","Entrar","02-10-2014");
INSERT INTO sv_logs VALUES("30","00.000.012","Entrar","07-10-2014");
INSERT INTO sv_logs VALUES("31","00.000.012","Ver Listas de Informacion","07-10-2014");
INSERT INTO sv_logs VALUES("32","00.000.012","Salir","07-10-2014");
INSERT INTO sv_logs VALUES("33","00.000.012","Entrar","10-10-2014");
INSERT INTO sv_logs VALUES("34","00.000.012","Ver Listas de Usuarios","10-10-2014");
INSERT INTO sv_logs VALUES("35","00.000.012","Ver Listas de Usuarios","10-10-2014");
INSERT INTO sv_logs VALUES("36","00.000.012","Bloquear Usuario","10-10-2014");
INSERT INTO sv_logs VALUES("37","00.000.012","Ver Listas de Usuarios","10-10-2014");
INSERT INTO sv_logs VALUES("38","00.000.012","Borrar Usuario","10-10-2014");
INSERT INTO sv_logs VALUES("39","00.000.012","Ver Listas de Usuarios","10-10-2014");
INSERT INTO sv_logs VALUES("40","00.000.012","Ver Listas de Informacion","10-10-2014");
INSERT INTO sv_logs VALUES("41","00.000.012","Ver Listas de Informacion","10-10-2014");
INSERT INTO sv_logs VALUES("42","00.000.012","Entrar","11-10-2014");
INSERT INTO sv_logs VALUES("43","00.000.012","Ver Listas de Informacion","11-10-2014");
INSERT INTO sv_logs VALUES("44","00.000.012","Ver Listas de Informacion","11-10-2014");
INSERT INTO sv_logs VALUES("45","00.000.012","Ver Listas de Informacion","11-10-2014");
INSERT INTO sv_logs VALUES("46","00.000.012","Ver Listas de Informacion","11-10-2014");
INSERT INTO sv_logs VALUES("47","00.000.012","Ver Listas de Informacion","11-10-2014");
INSERT INTO sv_logs VALUES("48","00.000.012","Ver Todas plantillas","11-10-2014");
INSERT INTO sv_logs VALUES("49","00.000.012","Ver Todas plantillas","11-10-2014");
INSERT INTO sv_logs VALUES("50","00.000.012","Ver Todas plantillas","11-10-2014");
INSERT INTO sv_logs VALUES("51","00.000.012","Ver Todas plantillas","11-10-2014");
INSERT INTO sv_logs VALUES("52","00.000.012","Ver Todas plantillas","11-10-2014");
INSERT INTO sv_logs VALUES("53","00.000.012","Ver Todas plantillas","11-10-2014");
INSERT INTO sv_logs VALUES("54","00.000.012","Ver Todos","11-10-2014");
INSERT INTO sv_logs VALUES("55","00.000.012","Ver Todas plantillas","11-10-2014");
INSERT INTO sv_logs VALUES("56","00.000.012","Ver Todas plantillas","11-10-2014");
INSERT INTO sv_logs VALUES("57","00.000.012","Ver Todas plantillas","11-10-2014");
INSERT INTO sv_logs VALUES("58","00.000.012","Ver Todas plantillas","11-10-2014");
INSERT INTO sv_logs VALUES("59","00.000.012","Ver Todas plantillas","11-10-2014");
INSERT INTO sv_logs VALUES("60","00.000.012","Ver Todos","11-10-2014");
INSERT INTO sv_logs VALUES("61","00.000.012","Ver Todos","11-10-2014");
INSERT INTO sv_logs VALUES("62","00.000.012","Ver Todos","11-10-2014");
INSERT INTO sv_logs VALUES("63","00.000.012","Ver Todos","11-10-2014");
INSERT INTO sv_logs VALUES("64","00.000.012","Ver Perfil","11-10-2014");
INSERT INTO sv_logs VALUES("65","00.000.012","Modificar Perfil","11-10-2014");
INSERT INTO sv_logs VALUES("66","00.000.012","Salir","11-10-2014");
INSERT INTO sv_logs VALUES("67","00.000.012","Entrar","11-10-2014");
INSERT INTO sv_logs VALUES("68","00.000.012","Salir","11-10-2014");
INSERT INTO sv_logs VALUES("69","00.000.012","Entrar","11-10-2014");
INSERT INTO sv_logs VALUES("70","00.000.012","Salir","11-10-2014");
INSERT INTO sv_logs VALUES("71","00.000.012","Entrar","11-10-2014");
INSERT INTO sv_logs VALUES("72","00.000.012","Salir","11-10-2014");
INSERT INTO sv_logs VALUES("73","00.000.012","Entrar","11-10-2014");
INSERT INTO sv_logs VALUES("74","00.000.012","Entrar","12-10-2014");
INSERT INTO sv_logs VALUES("75","00.000.012","Entrar","12-10-2014");
INSERT INTO sv_logs VALUES("76","00.000.012","Respaldar BD","12-10-2014");
INSERT INTO sv_logs VALUES("77","00.000.012","Respaldar Ficheros","12-10-2014");
INSERT INTO sv_logs VALUES("78","00.000.012","Reporte Usuarios","12-10-2014");
INSERT INTO sv_logs VALUES("79","00.000.012","Respaldar Ficheros","12-10-2014");
INSERT INTO sv_logs VALUES("80","00.000.012","Reporte Archivos","12-10-2014");
INSERT INTO sv_logs VALUES("81","00.000.012","Respaldar Ficheros","12-10-2014");
INSERT INTO sv_logs VALUES("82","00.000.012","Reporte Logs","12-10-2014");
INSERT INTO sv_logs VALUES("83","00.000.012","Respaldar Ficheros","12-10-2014");
INSERT INTO sv_logs VALUES("84","00.000.012","Entrar","13-10-2014");
INSERT INTO sv_logs VALUES("85","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("86","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("87","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("88","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("89","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("90","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("91","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("92","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("93","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("94","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("95","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("96","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("97","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("98","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("99","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("100","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("101","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("102","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("103","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("104","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("105","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("106","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("107","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("108","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("109","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("110","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("111","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("112","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("113","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("114","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("115","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("116","00.000.012","Ver Perfil","13-10-2014");
INSERT INTO sv_logs VALUES("117","00.000.012","Ver Perfil","13-10-2014");
INSERT INTO sv_logs VALUES("118","00.000.012","Respaldar Ficheros","13-10-2014");
INSERT INTO sv_logs VALUES("119","00.000.012","Salir","13-10-2014");
INSERT INTO sv_logs VALUES("120","00.000.012","Entrar","13-10-2014");
INSERT INTO sv_logs VALUES("121","00.000.012","Respaldar BD","13-10-2014");
INSERT INTO sv_logs VALUES("122","00.000.012","Respaldar Ficheros","13-10-2014");
INSERT INTO sv_logs VALUES("123","00.000.012","Reporte Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("124","00.000.012","Respaldar Ficheros","13-10-2014");
INSERT INTO sv_logs VALUES("125","00.000.012","Salir","13-10-2014");
INSERT INTO sv_logs VALUES("126","00.000.012","Entrar","13-10-2014");
INSERT INTO sv_logs VALUES("127","00.000.012","Respaldar BD","13-10-2014");
INSERT INTO sv_logs VALUES("128","00.000.012","Respaldar Ficheros","13-10-2014");
INSERT INTO sv_logs VALUES("129","00.000.012","Reporte Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("130","00.000.012","Respaldar Ficheros","13-10-2014");
INSERT INTO sv_logs VALUES("131","00.000.012","Reporte Archivos","13-10-2014");
INSERT INTO sv_logs VALUES("132","00.000.012","Respaldar Ficheros","13-10-2014");
INSERT INTO sv_logs VALUES("133","00.000.012","Reporte Logs","13-10-2014");
INSERT INTO sv_logs VALUES("134","00.000.012","Respaldar Ficheros","13-10-2014");
INSERT INTO sv_logs VALUES("135","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("136","00.000.012","Desbloquear Usuario","13-10-2014");
INSERT INTO sv_logs VALUES("137","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("138","00.000.012","Bloquear Usuario","13-10-2014");
INSERT INTO sv_logs VALUES("139","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("140","00.000.012","Bloquear Usuario","13-10-2014");
INSERT INTO sv_logs VALUES("141","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("142","00.000.012","Desbloquear Usuario","13-10-2014");
INSERT INTO sv_logs VALUES("143","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("144","00.000.012","Borrar Usuario","13-10-2014");
INSERT INTO sv_logs VALUES("145","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("146","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("147","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("148","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("149","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("150","00.000.012","Desbloquear Usuario","13-10-2014");
INSERT INTO sv_logs VALUES("151","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("152","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("153","00.000.012","Bloquear Usuario","13-10-2014");
INSERT INTO sv_logs VALUES("154","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("155","00.000.012","Bloquear Usuario","13-10-2014");
INSERT INTO sv_logs VALUES("156","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("157","00.000.012","Desbloquear Usuario","13-10-2014");
INSERT INTO sv_logs VALUES("158","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("159","00.000.012","Desbloquear Usuario","13-10-2014");
INSERT INTO sv_logs VALUES("160","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("161","00.000.012","Bloquear Usuario","13-10-2014");
INSERT INTO sv_logs VALUES("162","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("163","00.000.012","Borrar Usuario","13-10-2014");
INSERT INTO sv_logs VALUES("164","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("165","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("166","00.000.012","Ver Listas de Informacion","13-10-2014");
INSERT INTO sv_logs VALUES("167","00.000.012","Ver Listas de Informacion","13-10-2014");
INSERT INTO sv_logs VALUES("168","00.000.012","Ver Listas de Informacion","13-10-2014");
INSERT INTO sv_logs VALUES("169","00.000.012","Ver Listas de Informacion","13-10-2014");
INSERT INTO sv_logs VALUES("170","00.000.012","Ver Todas plantillas","13-10-2014");
INSERT INTO sv_logs VALUES("171","00.000.012","Ver Todos","13-10-2014");
INSERT INTO sv_logs VALUES("172","00.000.012","Ver Todas plantillas","13-10-2014");
INSERT INTO sv_logs VALUES("173","00.000.012","Ver Todas plantillas","13-10-2014");
INSERT INTO sv_logs VALUES("174","00.000.012","Ver Todas plantillas","13-10-2014");
INSERT INTO sv_logs VALUES("175","00.000.012","Ver Todas plantillas","13-10-2014");
INSERT INTO sv_logs VALUES("176","00.000.012","Ver Todas plantillas","13-10-2014");
INSERT INTO sv_logs VALUES("177","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("178","00.000.012","Ver Listas de Informacion","13-10-2014");
INSERT INTO sv_logs VALUES("179","00.000.012","Ver Listas de Informacion","13-10-2014");
INSERT INTO sv_logs VALUES("180","00.000.012","Ver Listas de Informacion","13-10-2014");
INSERT INTO sv_logs VALUES("181","00.000.012","Ver Listas de Informacion","13-10-2014");
INSERT INTO sv_logs VALUES("182","00.000.012","Ver Todas plantillas","13-10-2014");
INSERT INTO sv_logs VALUES("183","00.000.012","Ver Todas plantillas","13-10-2014");
INSERT INTO sv_logs VALUES("184","00.000.012","Ver Todas plantillas","13-10-2014");
INSERT INTO sv_logs VALUES("185","00.000.012","Ver Todas plantillas","13-10-2014");
INSERT INTO sv_logs VALUES("186","00.000.012","Ver Todas plantillas","13-10-2014");
INSERT INTO sv_logs VALUES("187","00.000.012","Ver Todas plantillas","13-10-2014");
INSERT INTO sv_logs VALUES("188","00.000.012","Ver Todas plantillas","13-10-2014");
INSERT INTO sv_logs VALUES("189","00.000.012","Ver Todas plantillas","13-10-2014");
INSERT INTO sv_logs VALUES("190","00.000.012","Ver Todas plantillas","13-10-2014");
INSERT INTO sv_logs VALUES("191","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("192","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("193","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("194","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("195","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("196","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("197","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("198","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("199","00.000.012","Ver Todos","13-10-2014");
INSERT INTO sv_logs VALUES("200","00.000.012","Ver Todos","13-10-2014");
INSERT INTO sv_logs VALUES("201","00.000.012","Ver Todos","13-10-2014");
INSERT INTO sv_logs VALUES("202","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("203","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("204","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("205","00.000.012","Ver Todos","13-10-2014");
INSERT INTO sv_logs VALUES("206","00.000.012","Ver Todos","13-10-2014");
INSERT INTO sv_logs VALUES("207","00.000.012","Ver Todos","13-10-2014");
INSERT INTO sv_logs VALUES("208","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("209","00.000.012","Ver Todos","13-10-2014");
INSERT INTO sv_logs VALUES("210","00.000.012","Ver Todos","13-10-2014");
INSERT INTO sv_logs VALUES("211","00.000.012","Ver Todos","13-10-2014");
INSERT INTO sv_logs VALUES("212","00.000.012","Ver Todos","13-10-2014");
INSERT INTO sv_logs VALUES("213","00.000.012","Ver Todos","13-10-2014");
INSERT INTO sv_logs VALUES("214","00.000.012","Ver Perfil","13-10-2014");
INSERT INTO sv_logs VALUES("215","00.000.012","Modificar Perfil","13-10-2014");
INSERT INTO sv_logs VALUES("216","00.000.012","Salir","13-10-2014");
INSERT INTO sv_logs VALUES("217","00.000.012","Entrar","13-10-2014");
INSERT INTO sv_logs VALUES("218","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("219","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("220","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("221","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("222","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("223","00.000.012","Bloquear Usuario","13-10-2014");
INSERT INTO sv_logs VALUES("224","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("225","00.000.012","Salir","13-10-2014");
INSERT INTO sv_logs VALUES("226","00.000.012","Entrar","13-10-2014");
INSERT INTO sv_logs VALUES("227","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("228","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("229","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("230","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("231","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("232","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("233","00.000.012","Salir","13-10-2014");
INSERT INTO sv_logs VALUES("234","00.000.012","Entrar","13-10-2014");
INSERT INTO sv_logs VALUES("235","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("236","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("237","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("238","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("239","00.000.012","Ver Perfil","13-10-2014");
INSERT INTO sv_logs VALUES("240","00.000.012","Ver Perfil","13-10-2014");
INSERT INTO sv_logs VALUES("241","00.000.012","Modificar Perfil","13-10-2014");
INSERT INTO sv_logs VALUES("242","00.000.012","Salir","13-10-2014");
INSERT INTO sv_logs VALUES("243","00.000.012","Entrar","13-10-2014");
INSERT INTO sv_logs VALUES("244","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("245","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("246","00.000.012","Desbloquear Usuario","13-10-2014");
INSERT INTO sv_logs VALUES("247","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("248","00.000.012","Bloquear Usuario","13-10-2014");
INSERT INTO sv_logs VALUES("249","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("250","00.000.012","Salir","13-10-2014");
INSERT INTO sv_logs VALUES("251","00.000.012","Entrar","13-10-2014");
INSERT INTO sv_logs VALUES("252","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("253","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("254","00.000.012","Desbloquear Usuario","13-10-2014");
INSERT INTO sv_logs VALUES("255","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("256","00.000.012","Salir","13-10-2014");
INSERT INTO sv_logs VALUES("257","00.000.013","Entrar","13-10-2014");
INSERT INTO sv_logs VALUES("258","00.000.013","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("259","00.000.013","Ver Perfil","13-10-2014");
INSERT INTO sv_logs VALUES("260","00.000.013","Modificar Perfil","13-10-2014");
INSERT INTO sv_logs VALUES("261","00.000.013","Salir","13-10-2014");
INSERT INTO sv_logs VALUES("262","00.000.013","Entrar","13-10-2014");
INSERT INTO sv_logs VALUES("263","00.000.013","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("264","00.000.013","Salir","13-10-2014");
INSERT INTO sv_logs VALUES("265","00.000.012","Entrar","13-10-2014");
INSERT INTO sv_logs VALUES("266","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("267","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("268","00.000.012","Bloquear Usuario","13-10-2014");
INSERT INTO sv_logs VALUES("269","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("270","00.000.012","Salir","13-10-2014");
INSERT INTO sv_logs VALUES("271","00.000.012","Entrar","13-10-2014");
INSERT INTO sv_logs VALUES("272","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("273","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("274","00.000.012","Desbloquear Usuario","13-10-2014");
INSERT INTO sv_logs VALUES("275","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("276","00.000.012","Salir","13-10-2014");
INSERT INTO sv_logs VALUES("277","00.000.013","Entrar","13-10-2014");
INSERT INTO sv_logs VALUES("278","00.000.013","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("279","00.000.013","Ver Perfil","13-10-2014");
INSERT INTO sv_logs VALUES("280","00.000.013","Modificar Perfil","13-10-2014");
INSERT INTO sv_logs VALUES("281","00.000.013","Salir","13-10-2014");
INSERT INTO sv_logs VALUES("282","00.000.013","Entrar","13-10-2014");
INSERT INTO sv_logs VALUES("283","00.000.013","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("284","00.000.013","Ver Perfil","13-10-2014");
INSERT INTO sv_logs VALUES("285","00.000.013","Modificar Perfil","13-10-2014");
INSERT INTO sv_logs VALUES("286","00.000.013","Salir","13-10-2014");
INSERT INTO sv_logs VALUES("287","00.000.013","Entrar","13-10-2014");
INSERT INTO sv_logs VALUES("288","00.000.013","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("289","00.000.013","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("290","00.000.013","Salir","13-10-2014");
INSERT INTO sv_logs VALUES("291","00.000.012","Entrar","13-10-2014");
INSERT INTO sv_logs VALUES("292","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("293","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("294","00.000.012","Bloquear Usuario","13-10-2014");
INSERT INTO sv_logs VALUES("295","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("296","00.000.012","Salir","13-10-2014");
INSERT INTO sv_logs VALUES("297","00.000.012","Entrar","13-10-2014");
INSERT INTO sv_logs VALUES("298","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("299","00.000.012","Salir","13-10-2014");
INSERT INTO sv_logs VALUES("300","00.000.012","Entrar","13-10-2014");
INSERT INTO sv_logs VALUES("301","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("302","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("303","00.000.012","Desbloquear Usuario","13-10-2014");
INSERT INTO sv_logs VALUES("304","00.000.012","Ver Listas de Usuarios","13-10-2014");
INSERT INTO sv_logs VALUES("305","00.000.012","Salir","13-10-2014");
INSERT INTO sv_logs VALUES("306","00.000.013","Entrar","13-10-2014");
INSERT INTO sv_logs VALUES("307","00.000.013","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("308","00.000.013","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("309","00.000.013","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("310","00.000.013","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("311","00.000.013","Salir","13-10-2014");
INSERT INTO sv_logs VALUES("312","00.000.014","Entrar","13-10-2014");
INSERT INTO sv_logs VALUES("313","00.000.014","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("314","00.000.014","Salir","13-10-2014");
INSERT INTO sv_logs VALUES("315","00.000.012","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("316","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("317","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("318","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("319","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("320","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("321","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("322","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("323","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("324","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("325","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("326","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("327","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("328","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("329","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("330","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("331","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("332","00.000.012","Ver Perfil","14-10-2014");
INSERT INTO sv_logs VALUES("333","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("334","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("335","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("336","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("337","00.000.012","Ver Todos","14-10-2014");
INSERT INTO sv_logs VALUES("338","00.000.012","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("339","00.000.012","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("340","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("341","00.000.012","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("342","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("343","00.000.012","Ver Todos","14-10-2014");
INSERT INTO sv_logs VALUES("344","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("345","00.000.012","Ver Todos","14-10-2014");
INSERT INTO sv_logs VALUES("346","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("347","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("348","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("349","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("350","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("351","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("352","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("353","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("354","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("355","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("356","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("357","00.000.012","Buscar Libros","14-10-2014");
INSERT INTO sv_logs VALUES("358","00.000.012","Buscar Libros","14-10-2014");
INSERT INTO sv_logs VALUES("359","00.000.012","Buscar Libros","14-10-2014");
INSERT INTO sv_logs VALUES("360","00.000.012","Buscar Libros","14-10-2014");
INSERT INTO sv_logs VALUES("361","00.000.012","Buscar Libros","14-10-2014");
INSERT INTO sv_logs VALUES("362","00.000.012","Buscar Libros","14-10-2014");
INSERT INTO sv_logs VALUES("363","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("364","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("365","00.000.012","Buscar Libros","14-10-2014");
INSERT INTO sv_logs VALUES("366","00.000.012","Buscar Libros","14-10-2014");
INSERT INTO sv_logs VALUES("367","00.000.012","Buscar Libros","14-10-2014");
INSERT INTO sv_logs VALUES("368","00.000.012","Buscar Libros","14-10-2014");
INSERT INTO sv_logs VALUES("369","00.000.012","Buscar Libros","14-10-2014");
INSERT INTO sv_logs VALUES("370","00.000.012","Buscar Libros","14-10-2014");
INSERT INTO sv_logs VALUES("371","00.000.012","Buscar Libros","14-10-2014");
INSERT INTO sv_logs VALUES("372","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("373","00.000.012","Ver Todos","14-10-2014");
INSERT INTO sv_logs VALUES("374","00.000.012","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("375","00.000.013","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("376","00.000.013","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("377","00.000.013","Ver Perfil","14-10-2014");
INSERT INTO sv_logs VALUES("378","00.000.013","Modificar Perfil","14-10-2014");
INSERT INTO sv_logs VALUES("379","00.000.013","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("380","00.000.013","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("381","00.000.013","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("382","00.000.013","Ver Perfil","14-10-2014");
INSERT INTO sv_logs VALUES("383","00.000.013","Modificar Perfil","14-10-2014");
INSERT INTO sv_logs VALUES("384","00.000.013","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("385","00.000.013","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("386","00.000.013","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("387","00.000.013","Ver Perfil","14-10-2014");
INSERT INTO sv_logs VALUES("388","00.000.013","Modificar Perfil","14-10-2014");
INSERT INTO sv_logs VALUES("389","00.000.013","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("390","00.000.013","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("391","00.000.013","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("392","00.000.013","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("393","00.000.013","Ver Perfil","14-10-2014");
INSERT INTO sv_logs VALUES("394","00.000.013","Ver Perfil","14-10-2014");
INSERT INTO sv_logs VALUES("395","00.000.013","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("396","00.000.013","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("397","00.000.013","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("398","00.000.013","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("399","00.000.013","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("400","00.000.013","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("401","00.000.013","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("402","00.000.013","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("403","00.000.013","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("404","00.000.013","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("405","00.000.013","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("406","00.000.013","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("407","00.000.012","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("408","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("409","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("410","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("411","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("412","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("413","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("414","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("415","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("416","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("417","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("418","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("419","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("420","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("421","00.000.012","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("422","00.000.012","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("423","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("424","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("425","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("426","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("427","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("428","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("429","00.000.012","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("430","00.000.012","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("431","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("432","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("433","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("434","00.000.012","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("435","00.000.012","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("436","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("437","00.000.012","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("438","00.000.003","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("439","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("440","00.000.003","Ver Perfil","14-10-2014");
INSERT INTO sv_logs VALUES("441","00.000.003","Modificar Perfil","14-10-2014");
INSERT INTO sv_logs VALUES("442","00.000.003","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("443","00.000.003","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("444","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("445","00.000.003","Ver Perfil","14-10-2014");
INSERT INTO sv_logs VALUES("446","00.000.003","Ver Perfil","14-10-2014");
INSERT INTO sv_logs VALUES("447","00.000.003","Ver Perfil","14-10-2014");
INSERT INTO sv_logs VALUES("448","00.000.003","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("449","21.056.713","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("450","21.056.713","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("451","21.056.713","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("452","21.056.713","Ver Perfil","14-10-2014");
INSERT INTO sv_logs VALUES("453","21.056.713","Modificar Perfil","14-10-2014");
INSERT INTO sv_logs VALUES("454","21.056.713","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("455","21.056.713","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("456","21.056.713","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("457","21.056.713","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("458","21.056.713","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("459","00.000.003","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("460","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("461","00.000.003","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("462","00.000.012","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("463","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("464","00.000.012","Ver Listas de Usuarios","14-10-2014");
INSERT INTO sv_logs VALUES("465","00.000.012","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("466","00.000.003","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("467","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("468","00.000.003","Ver Listas de Usuarios","14-10-2014");
INSERT INTO sv_logs VALUES("469","00.000.003","Ver Listas de Usuarios","14-10-2014");
INSERT INTO sv_logs VALUES("470","00.000.003","Ver Listas de Usuarios","14-10-2014");
INSERT INTO sv_logs VALUES("471","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("472","00.000.003","Ver Listas de Usuarios","14-10-2014");
INSERT INTO sv_logs VALUES("473","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("474","00.000.003","Ver Listas de Usuarios","14-10-2014");
INSERT INTO sv_logs VALUES("475","00.000.003","Bloquear Usuario","14-10-2014");
INSERT INTO sv_logs VALUES("476","00.000.003","Ver Listas de Usuarios","14-10-2014");
INSERT INTO sv_logs VALUES("477","00.000.003","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("478","00.000.012","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("479","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("480","00.000.012","Ver Listas de Usuarios","14-10-2014");
INSERT INTO sv_logs VALUES("481","00.000.012","Desbloquear Usuario","14-10-2014");
INSERT INTO sv_logs VALUES("482","00.000.012","Ver Listas de Usuarios","14-10-2014");
INSERT INTO sv_logs VALUES("483","00.000.012","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("484","00.000.003","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("485","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("486","00.000.003","Ver Todos","14-10-2014");
INSERT INTO sv_logs VALUES("487","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("488","00.000.003","Ver Todos","14-10-2014");
INSERT INTO sv_logs VALUES("489","00.000.003","Ver Todos","14-10-2014");
INSERT INTO sv_logs VALUES("490","00.000.003","Ver Todos","14-10-2014");
INSERT INTO sv_logs VALUES("491","00.000.003","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("492","00.000.012","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("493","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("494","00.000.012","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("495","00.000.012","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("496","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("497","00.000.012","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("498","00.000.003","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("499","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("500","00.000.003","Reporte Usuarios","14-10-2014");
INSERT INTO sv_logs VALUES("501","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("502","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("503","00.000.003","Reporte Archivos","14-10-2014");
INSERT INTO sv_logs VALUES("504","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("505","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("506","00.000.003","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("507","00.000.003","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("508","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("509","00.000.003","Reporte Logs","14-10-2014");
INSERT INTO sv_logs VALUES("510","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("511","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("512","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("513","00.000.003","Ver Listas de Usuarios","14-10-2014");
INSERT INTO sv_logs VALUES("514","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("515","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("516","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("517","00.000.003","Ver Todos","14-10-2014");
INSERT INTO sv_logs VALUES("518","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("519","00.000.003","Ver Todos","14-10-2014");
INSERT INTO sv_logs VALUES("520","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("521","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("522","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("523","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("524","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("525","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("526","00.000.003","Ver Todos","14-10-2014");
INSERT INTO sv_logs VALUES("527","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("528","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("529","00.000.003","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("530","21.056.713","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("531","21.056.713","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("532","21.056.713","Ver Perfil","14-10-2014");
INSERT INTO sv_logs VALUES("533","21.056.713","Ver Perfil","14-10-2014");
INSERT INTO sv_logs VALUES("534","21.056.713","Ver Perfil","14-10-2014");
INSERT INTO sv_logs VALUES("535","21.056.713","Ver Perfil","14-10-2014");
INSERT INTO sv_logs VALUES("536","21.056.713","Ver Perfil","14-10-2014");
INSERT INTO sv_logs VALUES("537","21.056.713","Ver Perfil","14-10-2014");
INSERT INTO sv_logs VALUES("538","21.056.713","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("539","00.000.012","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("540","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("541","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("542","00.000.012","Ver Listas de Usuarios","14-10-2014");
INSERT INTO sv_logs VALUES("543","00.000.012","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("544","21.056.713","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("545","21.056.713","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("546","21.056.713","Ver Perfil","14-10-2014");
INSERT INTO sv_logs VALUES("547","21.056.713","Ver Perfil","14-10-2014");
INSERT INTO sv_logs VALUES("548","21.056.713","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("549","00.000.003","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("550","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("551","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("552","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("553","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("554","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("555","00.000.003","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("556","21.056.713","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("557","21.056.713","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("558","21.056.713","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("559","00.000.012","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("560","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("561","00.000.012","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("562","00.000.012","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("563","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("564","00.000.012","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("565","00.000.003","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("566","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("567","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("568","00.000.003","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("569","00.000.003","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("570","00.000.012","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("571","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("572","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("573","00.000.012","Ver Todas plantillas","14-10-2014");
INSERT INTO sv_logs VALUES("574","00.000.012","Ver Listas de Informacion","14-10-2014");
INSERT INTO sv_logs VALUES("575","00.000.012","Respaldar BD","14-10-2014");
INSERT INTO sv_logs VALUES("576","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("577","00.000.012","Ver Listas de Informacion","14-10-2014");
INSERT INTO sv_logs VALUES("578","00.000.012","Ver Todas plantillas","14-10-2014");
INSERT INTO sv_logs VALUES("579","00.000.012","Respaldar BD","14-10-2014");
INSERT INTO sv_logs VALUES("580","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("581","00.000.012","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("582","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("583","00.000.012","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("584","00.000.012","Entrar","14-10-2014");
INSERT INTO sv_logs VALUES("585","00.000.012","Ver Libros Prestados","14-10-2014");
INSERT INTO sv_logs VALUES("586","00.000.012","Salir","14-10-2014");
INSERT INTO sv_logs VALUES("587","00.000.012","Entrar","15-10-2014");
INSERT INTO sv_logs VALUES("588","00.000.012","Ver Libros Prestados","15-10-2014");
INSERT INTO sv_logs VALUES("589","00.000.012","Respaldar BD","15-10-2014");
INSERT INTO sv_logs VALUES("590","00.000.012","Ver Libros Prestados","15-10-2014");
INSERT INTO sv_logs VALUES("591","00.000.012","Ver Libros Prestados","15-10-2014");
INSERT INTO sv_logs VALUES("592","00.000.012","Respaldar BD","15-10-2014");
INSERT INTO sv_logs VALUES("593","00.000.012","Ver Libros Prestados","15-10-2014");
INSERT INTO sv_logs VALUES("594","00.000.012","Ver Libros Prestados","15-10-2014");
INSERT INTO sv_logs VALUES("595","00.000.012","Ver Libros Prestados","15-10-2014");
INSERT INTO sv_logs VALUES("596","00.000.012","Respaldar BD","15-10-2014");
INSERT INTO sv_logs VALUES("597","00.000.012","Ver Libros Prestados","15-10-2014");
INSERT INTO sv_logs VALUES("598","00.000.000","Ver Libros Prestados","16-10-2014");
INSERT INTO sv_logs VALUES("599","00.000.000","Salir","16-10-2014");
INSERT INTO sv_logs VALUES("600","00.000.012","Entrar","16-10-2014");
INSERT INTO sv_logs VALUES("601","00.000.012","Ver Libros Prestados","16-10-2014");
INSERT INTO sv_logs VALUES("602","00.000.013","Entrar","17-10-2014");
INSERT INTO sv_logs VALUES("603","00.000.013","Ver Libros Prestados","17-10-2014");
INSERT INTO sv_logs VALUES("604","00.000.013","Ver Libros Prestados","17-10-2014");
INSERT INTO sv_logs VALUES("605","00.000.013","Ver Libros Prestados","17-10-2014");
INSERT INTO sv_logs VALUES("606","00.000.012","Entrar","18-10-2014");
INSERT INTO sv_logs VALUES("607","00.000.012","Ver Libros Prestados","18-10-2014");
INSERT INTO sv_logs VALUES("608","00.000.012","Entrar","19-10-2014");
INSERT INTO sv_logs VALUES("609","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("610","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("611","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("612","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("613","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("614","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("615","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("616","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("617","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("618","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("619","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("620","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("621","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("622","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("623","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("624","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("625","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("626","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("627","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("628","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("629","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("630","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("631","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("632","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("633","00.000.012","Salir","19-10-2014");
INSERT INTO sv_logs VALUES("634","00.000.013","Entrar","19-10-2014");
INSERT INTO sv_logs VALUES("635","00.000.013","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("636","00.000.013","Salir","19-10-2014");
INSERT INTO sv_logs VALUES("637","00.000.012","Entrar","19-10-2014");
INSERT INTO sv_logs VALUES("638","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("639","00.000.012","Salir","19-10-2014");
INSERT INTO sv_logs VALUES("640","00.000.013","Entrar","19-10-2014");
INSERT INTO sv_logs VALUES("641","00.000.013","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("642","00.000.013","Salir","19-10-2014");
INSERT INTO sv_logs VALUES("643","00.000.012","Entrar","19-10-2014");
INSERT INTO sv_logs VALUES("644","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("645","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("646","00.000.012","Salir","19-10-2014");
INSERT INTO sv_logs VALUES("647","00.000.013","Entrar","19-10-2014");
INSERT INTO sv_logs VALUES("648","00.000.013","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("649","00.000.013","Ver Perfil","19-10-2014");
INSERT INTO sv_logs VALUES("650","00.000.013","Ver Perfil","19-10-2014");
INSERT INTO sv_logs VALUES("651","00.000.013","Ver Perfil","19-10-2014");
INSERT INTO sv_logs VALUES("652","00.000.013","Ver Perfil","19-10-2014");
INSERT INTO sv_logs VALUES("653","00.000.013","Ver Perfil","19-10-2014");
INSERT INTO sv_logs VALUES("654","00.000.013","Modificar Perfil","19-10-2014");
INSERT INTO sv_logs VALUES("655","00.000.013","Salir","19-10-2014");
INSERT INTO sv_logs VALUES("656","00.000.013","Entrar","19-10-2014");
INSERT INTO sv_logs VALUES("657","00.000.013","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("658","00.000.013","Ver Perfil","19-10-2014");
INSERT INTO sv_logs VALUES("659","00.000.013","Ver Perfil","19-10-2014");
INSERT INTO sv_logs VALUES("660","00.000.013","Ver Perfil","19-10-2014");
INSERT INTO sv_logs VALUES("661","00.000.013","Ver Perfil","19-10-2014");
INSERT INTO sv_logs VALUES("662","00.000.013","Ver Perfil","19-10-2014");
INSERT INTO sv_logs VALUES("663","00.000.013","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("664","00.000.013","Salir","19-10-2014");
INSERT INTO sv_logs VALUES("665","00.000.013","Entrar","19-10-2014");
INSERT INTO sv_logs VALUES("666","00.000.013","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("667","00.000.013","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("668","00.000.013","Salir","19-10-2014");
INSERT INTO sv_logs VALUES("669","00.000.012","Entrar","19-10-2014");
INSERT INTO sv_logs VALUES("670","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("671","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("672","00.000.012","Ver Perfil","19-10-2014");
INSERT INTO sv_logs VALUES("673","00.000.012","Modificar Perfil","19-10-2014");
INSERT INTO sv_logs VALUES("674","00.000.012","Salir","19-10-2014");
INSERT INTO sv_logs VALUES("675","00.000.012","Entrar","19-10-2014");
INSERT INTO sv_logs VALUES("676","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("677","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("678","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("679","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("680","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("681","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("682","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("683","00.000.012","Ver Perfil","19-10-2014");
INSERT INTO sv_logs VALUES("684","00.000.012","Modificar Perfil","19-10-2014");
INSERT INTO sv_logs VALUES("685","00.000.012","Salir","19-10-2014");
INSERT INTO sv_logs VALUES("686","00.000.012","Entrar","19-10-2014");
INSERT INTO sv_logs VALUES("687","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("688","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("689","00.000.012","Salir","19-10-2014");
INSERT INTO sv_logs VALUES("690","00.000.012","Entrar","19-10-2014");
INSERT INTO sv_logs VALUES("691","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("692","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("693","00.000.012","Ver Listas de Informacion","19-10-2014");
INSERT INTO sv_logs VALUES("694","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("695","00.000.012","Ver Libros Prestados","19-10-2014");
INSERT INTO sv_logs VALUES("696","00.000.012","Respaldar BD","19-10-2014");
INSERT INTO sv_logs VALUES("697","00.000.012","Ver Libros Prestados","19-10-2014");



DROP TABLE IF EXISTS sv_plantillas;

CREATE TABLE `sv_plantillas` (
  `id` int(255) NOT NULL auto_increment,
  `id_user` int(255) NOT NULL,
  `tipo` varchar(15) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `descripcion` varchar(140) NOT NULL,
  `fecha_registro` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO sv_plantillas VALUES("2","1","SMS","alerta 25 dias","usuario le quedan 25 dias para entregar el libro","11-10-2014");
INSERT INTO sv_plantillas VALUES("3","1","SMS","aviso 1","aviso te quedan 1 para entregar libro","14-10-2014");



DROP TABLE IF EXISTS sv_prestamos;

CREATE TABLE `sv_prestamos` (
  `id` int(255) NOT NULL auto_increment,
  `id_user` int(255) NOT NULL,
  `id_libro` int(255) NOT NULL,
  `id_bibliotecario` int(255) NOT NULL,
  `status` varchar(10) NOT NULL,
  `cant_dias_prestamo` varchar(2) NOT NULL,
  `fecha_prestamo_retorno` varchar(10) NOT NULL,
  `fecha_registro` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `id_libro` (`id_libro`),
  CONSTRAINT `sv_prestamos_ibfk_1` FOREIGN KEY (`id_libro`) REFERENCES `sv_libros` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

INSERT INTO sv_prestamos VALUES("4","1","4","1","Entregado","12","2014-10-13","13-10-2014");
INSERT INTO sv_prestamos VALUES("10","1","7","1","Entregado","15","2014-10-28","13-10-2014");
INSERT INTO sv_prestamos VALUES("11","1","3","1","Entregado","14","2014-10-28","14-10-2014");
INSERT INTO sv_prestamos VALUES("12","1","3","5","Entregado","12","2014-10-29","14-10-2014");
INSERT INTO sv_prestamos VALUES("13","105","5","5","Prestado","12","","14-10-2014");
INSERT INTO sv_prestamos VALUES("14","1","3","5","Prestado","12","","14-10-2014");
INSERT INTO sv_prestamos VALUES("15","106","7","5","Prestado","3","","14-10-2014");



DROP TABLE IF EXISTS sv_slides;

CREATE TABLE `sv_slides` (
  `id` int(255) NOT NULL default '0',
  `titulo` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `url_images` text NOT NULL,
  `autor` int(255) NOT NULL,
  `fecha` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE IF EXISTS sv_users;

CREATE TABLE `sv_users` (
  `id` int(255) NOT NULL auto_increment,
  `nacionalidad` varchar(1) NOT NULL,
  `ci` varchar(27) NOT NULL,
  `nombre` varchar(15) NOT NULL,
  `apellido` varchar(15) NOT NULL,
  `usuario` varchar(15) NOT NULL,
  `clave` varchar(21) NOT NULL,
  `fecha_nac` varchar(10) NOT NULL,
  `tipo_user` varchar(15) NOT NULL,
  `estado` varchar(10) NOT NULL,
  `correo` varchar(150) NOT NULL,
  `telefono` varchar(12) NOT NULL,
  `ruta_img` varchar(200) NOT NULL,
  `pregunta` varchar(30) NOT NULL,
  `respuesta` varchar(30) NOT NULL,
  `fecha_registro` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `usuario` (`usuario`),
  UNIQUE KEY `correo` (`correo`),
  UNIQUE KEY `ci` (`ci`)
) ENGINE=MyISAM AUTO_INCREMENT=107 DEFAULT CHARSET=utf8;

INSERT INTO sv_users VALUES("1","V","00.000.012","admin","admin","admin","123456","20-09-1990","admin","activo","warlhunters@gmail.com","04125272957","descarg.png","cual es mi gato","calesita","28-04-2014");
INSERT INTO sv_users VALUES("5","V","00.000.003","mario","cegobia","bibliotecario","bibliotecario","10-09-1989","bibliotecario","activo","mario@biblioteca.com","","Captura de pantalla de 2014-10-01 11:35:08.png","","","29-05-2014");
INSERT INTO sv_users VALUES("103","V","00.000.013","usuario","usuario","usuario","usuario","1983-12-31","users","activo","usuario@biblioteca.com","584245713467","descarga.jpg","","","13-10-2014");
INSERT INTO sv_users VALUES("104","V","00.000.014","oso","oso","oso","123456","1991-10-01","users","activo","as@biblioteca.com","0423-542-121","","","","13-10-2014");
INSERT INTO sv_users VALUES("106","V","21.056.713","laura","pascualis","laura","123456","2000-12-31","users","activo","laura@biblioteca.com","04121539069","","","","14-10-2014");



