CREATE DATABASE IF NOT EXISTS biblioteca;

USE biblioteca;

DROP TABLE IF EXISTS sv_configuracion;

CREATE TABLE `sv_configuracion` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `telefono` varchar(14) NOT NULL,
  ` correo` varchar(150) NOT NULL,
  `facebook` varchar(150) NOT NULL,
  `twitter` varchar(150) NOT NULL,
  `url_logo` varchar(150) NOT NULL,
  `url_fondo` varchar(200) NOT NULL,
  `autor` int(255) NOT NULL,
  `fecha_registro` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO sv_configuracion VALUES("1","Biblioteca Publica Andrés Eloy Blanco","Barinas Estado Barinas  Centro de Barinas con Avenida Brinceño Méndez cruce con Calle Carvaja","0273-215-09-02","bibliotecapublica-aeb@hotmail.com","","@BPAndresEloyBlanco","http://localhost/biblioteca/www/lib/themes/biblioteca/img/logo.png","","0","19-07-2014");



DROP TABLE IF EXISTS sv_dewey;

CREATE TABLE `sv_dewey` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(3) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `descripcion` text NOT NULL,
  `autor` int(255) NOT NULL,
  `fecha_registro` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

INSERT INTO sv_dewey VALUES("1","00","Obras Generales","Obras Generales","0","19-07-2014");
INSERT INTO sv_dewey VALUES("2","100","Filosofía y Psicología","Filosofía y Psicología","0","19-07-2014");
INSERT INTO sv_dewey VALUES("3","200","Religión","Religión","0","19-07-2014");
INSERT INTO sv_dewey VALUES("4","300","Ciencias Sociales","Ciencias Sociales (Sociología,  Política, Economía, Derecho, Premilitar, Educación, Folklore, Ambiente)","0","19-07-2014");
INSERT INTO sv_dewey VALUES("5","400","Lingüística","Lingüística (Ingles)","0","19-07-2014");
INSERT INTO sv_dewey VALUES("6","500","Ciencias Puras","Ciencias Puras (Matemática, Física, Química, Geología, Ciencia de la Tierra, Biología, Botánica, Salud, Dibujo Técnico, Medicina, Agricultura)","0","19-07-2014");
INSERT INTO sv_dewey VALUES("7","600","Ciencia Aplicada","Ciencia Aplicada (Administración)","0","19-07-2014");
INSERT INTO sv_dewey VALUES("8","700","Arte y Recreación","Arte y Recreación","0","19-07-2014");
INSERT INTO sv_dewey VALUES("9","800","Literatura","Literatura (Novelas, Novelas de Venezuela,  Poesía, Poesía de Venezuela, Teatro, Teatro de Venezuela)","0","19-07-2014");
INSERT INTO sv_dewey VALUES("10","900","Geografía","Geografía (Historia, Educación Artística, Geografía General, Geografía de Venezuela, Catedra de Bolivariana, Historia de Venezuela, Historia Universal)","0","19-07-2014");



DROP TABLE IF EXISTS sv_informacion;

CREATE TABLE `sv_informacion` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `descripcion` text NOT NULL,
  `tipo` varchar(20) NOT NULL,
  `autor` int(255) NOT NULL,
  `fecha` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO sv_informacion VALUES("1","historia","Barinas forma parte de la red de bibliotecas públicas desde 1977, para el mes de mayo se crea la red de bibliotecas públicas a través de decreto, y se funda la biblioteca pública “Andrés Eloy Blanco” y es puesta en funcionamiento para el público en general, en Enero de 1978.  Andrés Eloy Blanco es una institución pública perteneciente a la red de bibliotecas públicas del país, adscrita a la Secretaria Ejecutiva De Educación, bajo los lineamientos funcionales de biblioteca nacional, con el objetivo de seleccionar y organizar los textos bibliográficos que a ella llegan (Compra- Donación); con el fin de ponerlos a disposición del público en general, para que, todos los sectores tengan acceso a ella, y así cumplir con un derecho ciudadano que el estado garantiza.","historia","1","29-05-2014");
INSERT INTO sv_informacion VALUES("2","¿Para que Sirve?","Mediante este servicio, la Biblioteca te permite disponer de sus fondos bibliográficos, para su uso fuera de sus instalaciones por un periodo de tiempo determinado. Los usuarios podrán acceder a este servicio registrandose en la pagina y asi entregarles su carne que les acredite como tales","Publicidad","1","29-05-2014");
INSERT INTO sv_informacion VALUES("3","¿Quien tiene Acceso?","\n         Todo los usuarios con carné de la Biblioteca actualizado","publicidad","1","");
INSERT INTO sv_informacion VALUES("4","Condiciones del Prestamo","Los prestamos tendran un limite de 3 Dias sin execcion y solo si existen mas de 3 ejemplares\n  Si el libro de tu interes no esta disponible puedes reservarlo y cuando haya la disponibilidad se le avisará a través de mensaje de texto o correo electronico que te hayas registrado comprueba en los mostradores de información y préstamo que tenemos tu e-mail en nuestra base de datos","publicidad","1","29-05-2014");
INSERT INTO sv_informacion VALUES("5","Penalizacion","Una vez vencido el plazo de devolución, si la obra no ha sido devuelta, la autorización para nuevos préstamos queda suspendida. Se informará al usuario mediante un correo electrónico o mensaje de texto.\nLa penalización por la devolución de obras con retraso es de 1 mes de suspensión de carnet de prestamos \nSi la obra no ha sido devuelta después de tres avisos de reclamación, se cancelará el acceso del usuario a la Biblioteca hasta la devolución de la obra en préstamo.\nLa pérdida o deterioro de un libro supone la desautorización de nuevos préstamos hasta la reposición del mismo o, en su defecto, el abono de su importe.\nEl carné es personal e intransferible, por lo que su utilización por otra persona será sancionado.","publicidad","1","29-05-2014");



DROP TABLE IF EXISTS sv_libros;

CREATE TABLE `sv_libros` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `id_dewey` int(255) NOT NULL,
  `img_ruta` varchar(100) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `tags` varchar(255) NOT NULL,
  `autor` varchar(255) NOT NULL,
  `cantidad` int(2) NOT NULL,
  `usuario_id` int(255) NOT NULL,
  `fecha_registro` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_dewey` (`id_dewey`),
  CONSTRAINT `sv_libros_ibfk_1` FOREIGN KEY (`id_dewey`) REFERENCES `sv_dewey` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO sv_libros VALUES("1","1","banner_mercadopago.png","Creacion de Apis","web","sasds","c","0","1","29-09-2014");
INSERT INTO sv_libros VALUES("2","9","Captura de pantalla de 2014-05-17 19:15:28.png","juego de tronos","batallas epicas y muertes que jode","juego, tronos, guerra, otros","RR martin","0","1","29-09-2014");
INSERT INTO sv_libros VALUES("3","9","Captura de pantalla de 2014-03-16 03:56:04.png","cancion de fuego y hielo","batallas epicas y muertes que jode","asdasdas","RR martin","0","1","29-09-2014");
INSERT INTO sv_libros VALUES("4","9","Captura de pantalla de 2014-02-13 01:58:49.png","choque de reyes","batallas epicas y muertes que jode","ASDASDAS","RR martin","0","1","29-09-2014");
INSERT INTO sv_libros VALUES("5","9","Captura de pantalla de 2014-09-24 17:48:05.png","danza de dragones","batallas epicas y muertes que jode","asdas","RR martin","10","1","29-09-2014");
INSERT INTO sv_libros VALUES("6","9","","cancion de fuego y hielo","batallas epicas","batallas, novela, rrmartin","RR Martin","0","1","29-09-2014");
INSERT INTO sv_libros VALUES("7","7","conf_sistema.png","Ajax","JS Ajax libro","libro, ajax","1","4","1","11-10-2014");



DROP TABLE IF EXISTS sv_logs;

CREATE TABLE `sv_logs` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `ci_user` varchar(22) NOT NULL,
  `accion` varchar(35) NOT NULL,
  `fecha_registro` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=154 DEFAULT CHARSET=utf8;

INSERT INTO sv_logs VALUES("1","","Salir","29-09-2014");
INSERT INTO sv_logs VALUES("2","","Entrar","29-09-2014");
INSERT INTO sv_logs VALUES("3","","Reporte Logs","29-09-2014");
INSERT INTO sv_logs VALUES("4","","Salir","29-09-2014");
INSERT INTO sv_logs VALUES("5","00.000.012","Entrar","29-09-2014");
INSERT INTO sv_logs VALUES("6","00.000.012","Salir","29-09-2014");
INSERT INTO sv_logs VALUES("7","00.000.012","Entrar","29-09-2014");
INSERT INTO sv_logs VALUES("8","00.000.012","Entrar","29-09-2014");
INSERT INTO sv_logs VALUES("9","00.000.012","Salir","29-09-2014");
INSERT INTO sv_logs VALUES("10","00.000.012","Entrar","29-09-2014");
INSERT INTO sv_logs VALUES("11","00.000.012","Ver Todos","29-09-2014");
INSERT INTO sv_logs VALUES("12","00.000.012","Ver Todos","29-09-2014");
INSERT INTO sv_logs VALUES("13","00.000.012","Salir","29-09-2014");
INSERT INTO sv_logs VALUES("14","00.000.012","Entrar","30-09-2014");
INSERT INTO sv_logs VALUES("15","","Salir","30-09-2014");
INSERT INTO sv_logs VALUES("16","00.000.012","Entrar","30-09-2014");
INSERT INTO sv_logs VALUES("17","00.000.012","Agregar plantillas","30-09-2014");
INSERT INTO sv_logs VALUES("18","00.000.012","Entrar","01-10-2014");
INSERT INTO sv_logs VALUES("19","00.000.012","Agregar plantillas","01-10-2014");
INSERT INTO sv_logs VALUES("20","00.000.012","Agregar plantillas","01-10-2014");
INSERT INTO sv_logs VALUES("21","00.000.012","Agregar plantillas","01-10-2014");
INSERT INTO sv_logs VALUES("22","00.000.012","Ver Listas de Informacion","01-10-2014");
INSERT INTO sv_logs VALUES("23","00.000.012","Agregar plantillas","01-10-2014");
INSERT INTO sv_logs VALUES("24","00.000.012","Ver plantillas","01-10-2014");
INSERT INTO sv_logs VALUES("25","00.000.012","Ver plantillas","01-10-2014");
INSERT INTO sv_logs VALUES("26","00.000.012","Entrar","01-10-2014");
INSERT INTO sv_logs VALUES("27","00.000.012","Ver plantillas","01-10-2014");
INSERT INTO sv_logs VALUES("28","00.000.012","Ver plantillas","01-10-2014");
INSERT INTO sv_logs VALUES("29","00.000.012","Entrar","02-10-2014");
INSERT INTO sv_logs VALUES("30","00.000.012","Entrar","07-10-2014");
INSERT INTO sv_logs VALUES("31","00.000.012","Ver Listas de Informacion","07-10-2014");
INSERT INTO sv_logs VALUES("32","00.000.012","Salir","07-10-2014");
INSERT INTO sv_logs VALUES("33","00.000.012","Entrar","10-10-2014");
INSERT INTO sv_logs VALUES("34","00.000.012","Ver Listas de Usuarios","10-10-2014");
INSERT INTO sv_logs VALUES("35","00.000.012","Ver Listas de Usuarios","10-10-2014");
INSERT INTO sv_logs VALUES("36","00.000.012","Bloquear Usuario","10-10-2014");
INSERT INTO sv_logs VALUES("37","00.000.012","Ver Listas de Usuarios","10-10-2014");
INSERT INTO sv_logs VALUES("38","00.000.012","Borrar Usuario","10-10-2014");
INSERT INTO sv_logs VALUES("39","00.000.012","Ver Listas de Usuarios","10-10-2014");
INSERT INTO sv_logs VALUES("40","00.000.012","Ver Listas de Informacion","10-10-2014");
INSERT INTO sv_logs VALUES("41","00.000.012","Ver Listas de Informacion","10-10-2014");
INSERT INTO sv_logs VALUES("42","00.000.012","Entrar","11-10-2014");
INSERT INTO sv_logs VALUES("43","00.000.012","Ver Listas de Informacion","11-10-2014");
INSERT INTO sv_logs VALUES("44","00.000.012","Ver Listas de Informacion","11-10-2014");
INSERT INTO sv_logs VALUES("45","00.000.012","Ver Listas de Informacion","11-10-2014");
INSERT INTO sv_logs VALUES("46","00.000.012","Ver Listas de Informacion","11-10-2014");
INSERT INTO sv_logs VALUES("47","00.000.012","Ver Listas de Informacion","11-10-2014");
INSERT INTO sv_logs VALUES("48","00.000.012","Ver Todas plantillas","11-10-2014");
INSERT INTO sv_logs VALUES("49","00.000.012","Ver Todas plantillas","11-10-2014");
INSERT INTO sv_logs VALUES("50","00.000.012","Ver Todas plantillas","11-10-2014");
INSERT INTO sv_logs VALUES("51","00.000.012","Ver Todas plantillas","11-10-2014");
INSERT INTO sv_logs VALUES("52","00.000.012","Ver Todas plantillas","11-10-2014");
INSERT INTO sv_logs VALUES("53","00.000.012","Ver Todas plantillas","11-10-2014");
INSERT INTO sv_logs VALUES("54","00.000.012","Ver Todos","11-10-2014");
INSERT INTO sv_logs VALUES("55","00.000.012","Ver Todas plantillas","11-10-2014");
INSERT INTO sv_logs VALUES("56","00.000.012","Ver Todas plantillas","11-10-2014");
INSERT INTO sv_logs VALUES("57","00.000.012","Ver Todas plantillas","11-10-2014");
INSERT INTO sv_logs VALUES("58","00.000.012","Ver Todas plantillas","11-10-2014");
INSERT INTO sv_logs VALUES("59","00.000.012","Ver Todas plantillas","11-10-2014");
INSERT INTO sv_logs VALUES("60","00.000.012","Ver Todos","11-10-2014");
INSERT INTO sv_logs VALUES("61","00.000.012","Ver Todos","11-10-2014");
INSERT INTO sv_logs VALUES("62","00.000.012","Ver Todos","11-10-2014");
INSERT INTO sv_logs VALUES("63","00.000.012","Ver Todos","11-10-2014");
INSERT INTO sv_logs VALUES("64","00.000.012","Ver Perfil","11-10-2014");
INSERT INTO sv_logs VALUES("65","00.000.012","Modificar Perfil","11-10-2014");
INSERT INTO sv_logs VALUES("66","00.000.012","Salir","11-10-2014");
INSERT INTO sv_logs VALUES("67","00.000.012","Entrar","11-10-2014");
INSERT INTO sv_logs VALUES("68","00.000.012","Salir","11-10-2014");
INSERT INTO sv_logs VALUES("69","00.000.012","Entrar","11-10-2014");
INSERT INTO sv_logs VALUES("70","00.000.012","Salir","11-10-2014");
INSERT INTO sv_logs VALUES("71","00.000.012","Entrar","11-10-2014");
INSERT INTO sv_logs VALUES("72","00.000.012","Salir","11-10-2014");
INSERT INTO sv_logs VALUES("73","00.000.012","Entrar","11-10-2014");
INSERT INTO sv_logs VALUES("74","00.000.012","Entrar","12-10-2014");
INSERT INTO sv_logs VALUES("75","00.000.012","Entrar","12-10-2014");
INSERT INTO sv_logs VALUES("76","00.000.012","Respaldar BD","12-10-2014");
INSERT INTO sv_logs VALUES("77","00.000.012","Respaldar Ficheros","12-10-2014");
INSERT INTO sv_logs VALUES("78","00.000.012","Reporte Usuarios","12-10-2014");
INSERT INTO sv_logs VALUES("79","00.000.012","Respaldar Ficheros","12-10-2014");
INSERT INTO sv_logs VALUES("80","00.000.012","Reporte Archivos","12-10-2014");
INSERT INTO sv_logs VALUES("81","00.000.012","Respaldar Ficheros","12-10-2014");
INSERT INTO sv_logs VALUES("82","00.000.012","Reporte Logs","12-10-2014");
INSERT INTO sv_logs VALUES("83","00.000.012","Respaldar Ficheros","12-10-2014");
INSERT INTO sv_logs VALUES("84","00.000.012","Entrar","13-10-2014");
INSERT INTO sv_logs VALUES("85","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("86","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("87","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("88","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("89","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("90","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("91","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("92","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("93","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("94","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("95","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("96","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("97","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("98","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("99","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("100","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("101","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("102","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("103","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("104","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("105","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("106","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("107","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("108","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("109","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("110","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("111","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("112","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("113","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("114","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("115","00.000.012","Ver Libros Prestados","13-10-2014");
INSERT INTO sv_logs VALUES("116","00.000.012","Ver Perfil","13-10-2014");
INSERT INTO sv_logs VALUES("117","00.000.012","Ver Perfil","13-10-2014");
INSERT INTO sv_logs VALUES("118","00.000.012","Respaldar Ficheros","13-10-2014");
INSERT INTO sv_logs VALUES("119","00.000.012","Salir","13-10-2014");
INSERT INTO sv_logs VALUES("120","00.000.012","Entrar","13-10-2014");
INSERT INTO sv_logs VALUES("121","00.000.012","Respaldar BD","13-10-2014");
INSERT INTO sv_logs VALUES("122","00.000.012","Entrar","22-11-2014");
INSERT INTO sv_logs VALUES("123","00.000.012","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("124","00.000.012","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("125","00.000.012","Desbloquear Usuario","22-11-2014");
INSERT INTO sv_logs VALUES("126","00.000.012","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("127","00.000.012","Reporte Logs","22-11-2014");
INSERT INTO sv_logs VALUES("128","00.000.012","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("129","00.000.012","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("130","00.000.012","Ver Listas de Informacion","22-11-2014");
INSERT INTO sv_logs VALUES("131","00.000.012","Ver Perfil","22-11-2014");
INSERT INTO sv_logs VALUES("132","00.000.012","Ver Perfil","22-11-2014");
INSERT INTO sv_logs VALUES("133","00.000.012","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("134","00.000.012","Ver Todas plantillas","22-11-2014");
INSERT INTO sv_logs VALUES("135","00.000.012","Ver Todas plantillas","22-11-2014");
INSERT INTO sv_logs VALUES("136","00.000.012","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("137","00.000.012","Ver Todas plantillas","22-11-2014");
INSERT INTO sv_logs VALUES("138","00.000.012","Ver Perfil","22-11-2014");
INSERT INTO sv_logs VALUES("139","00.000.012","Ver Todas plantillas","22-11-2014");
INSERT INTO sv_logs VALUES("140","00.000.012","Ver Todas plantillas","22-11-2014");
INSERT INTO sv_logs VALUES("141","00.000.012","Ver Todas plantillas","22-11-2014");
INSERT INTO sv_logs VALUES("142","00.000.012","Ver Todas plantillas","22-11-2014");
INSERT INTO sv_logs VALUES("143","00.000.012","Ver Listas de Informacion","22-11-2014");
INSERT INTO sv_logs VALUES("144","00.000.012","Ver Listas de Informacion","22-11-2014");
INSERT INTO sv_logs VALUES("145","00.000.012","Ver Listas de Informacion","22-11-2014");
INSERT INTO sv_logs VALUES("146","00.000.012","Respaldar Ficheros","22-11-2014");
INSERT INTO sv_logs VALUES("147","00.000.012","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("148","00.000.012","Respaldar BD","22-11-2014");
INSERT INTO sv_logs VALUES("149","00.000.012","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("150","00.000.012","Respaldar Ficheros","22-11-2014");
INSERT INTO sv_logs VALUES("151","00.000.012","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("152","00.000.012","Respaldar BD","22-11-2014");
INSERT INTO sv_logs VALUES("153","00.000.012","Ver Libros Prestados","22-11-2014");



DROP TABLE IF EXISTS sv_plantillas;

CREATE TABLE `sv_plantillas` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `id_user` int(255) NOT NULL,
  `tipo` varchar(15) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `descripcion` varchar(140) NOT NULL,
  `fecha_registro` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO sv_plantillas VALUES("1","1","SMS","alerta 1 dias","usuario le quedan 1 dias para entregar el libro","01-10-2014");
INSERT INTO sv_plantillas VALUES("2","1","SMS","alerta 1 dias despues","usuario debe entregar el libro","11-10-2014");



DROP TABLE IF EXISTS sv_prestamos;

CREATE TABLE `sv_prestamos` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `id_user` int(255) NOT NULL,
  `id_libro` int(255) NOT NULL,
  `id_bibliotecario` int(255) NOT NULL,
  `status` varchar(10) NOT NULL,
  `cant_dias_prestamo` varchar(2) NOT NULL,
  `fecha_prestamo_retorno` varchar(10) NOT NULL,
  `fecha_registro` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_libro` (`id_libro`),
  CONSTRAINT `sv_prestamos_ibfk_1` FOREIGN KEY (`id_libro`) REFERENCES `sv_libros` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO sv_prestamos VALUES("4","1","7","1","Entregado","12","2014-10-13","13-10-2014");
INSERT INTO sv_prestamos VALUES("5","1","7","1","Prestado","15","2014-10-13","13-10-2014");



DROP TABLE IF EXISTS sv_slides;

CREATE TABLE `sv_slides` (
  `id` int(255) NOT NULL DEFAULT '0',
  `titulo` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `url_images` text NOT NULL,
  `autor` int(255) NOT NULL,
  `fecha` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE IF EXISTS sv_users;

CREATE TABLE `sv_users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nacionalidad` varchar(1) NOT NULL,
  `ci` varchar(27) NOT NULL,
  `nombre` varchar(15) NOT NULL,
  `apellido` varchar(15) NOT NULL,
  `usuario` varchar(15) NOT NULL,
  `clave` varchar(21) NOT NULL,
  `fecha_nac` varchar(10) NOT NULL,
  `tipo_user` varchar(15) NOT NULL,
  `estado` varchar(10) NOT NULL,
  `correo` varchar(150) NOT NULL,
  `telefono` varchar(12) NOT NULL,
  `ruta_img` varchar(200) NOT NULL,
  `fecha_registro` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario` (`usuario`),
  UNIQUE KEY `correo` (`correo`),
  UNIQUE KEY `ci` (`ci`)
) ENGINE=MyISAM AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;

INSERT INTO sv_users VALUES("1","V","00.000.012","admin","admin","admin","admin","20-09-1990","admin","activo","warlhunters@gmail.com","","libros_todos_administracion_buscar.png","28-04-2014");
INSERT INTO sv_users VALUES("5","V","00.000.003","mario","cegobia","bibliotecario","bibliotecario","10-09-1989","bibliotecario","activo","mario@sisven.com","","","29-05-2014");
INSERT INTO sv_users VALUES("101","V","00.000.009","usuario","usuario","usuario","usuario","1989-09-20","users","activo","usuuario@biblioteca.com","","","24-07-2014");



