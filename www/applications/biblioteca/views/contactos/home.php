		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Mis contactos </h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a title='Abrir Acciones' class='open-contacto btn btn-success bt-lg' href="#addcontacto">
                                <i class="fa fa-plus"></i>
                                Agregar
                            </a>
                            <a title='Abrir Acciones' class='open-borrar btn btn-success bt-lg' href="#listas">
                                <i class="fa fa-table"></i>
                                Listas
                            </a>
                            <a title='Abrir Acciones' class='open-borrar btn btn-danger bt-lg' href="#borrar">
                                <i class="fa fa-bitbucket"></i>
                                Borrar
                            </a>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Nombre y apellido</th>
                                            <th>Correo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            if(is_Array($contactos)){
                                                for ($i=0; $i <= $contactos[$i]["id"]; $i++) {
                                                    for ($j=0; $j <= $usuarios[$j]["id"] ; $j++) { 
                                                        if($contactos[$i]["user_id_amigo"]==$usuarios[$j]["id"]){
                                                            echo "<tr class='odd gradeX'>";
                                                            echo "<td>".$contactos[$i]['fecha_registro']."</td>";
                                                            echo "<td>".$usuarios[$j]['nombre']." ".$usuarios[$j]['apellido']."</td>";
                                                            echo "<td>".$usuarios[$j]['correo']."</td>";
                                                            echo "</tr>";
                                                        }
                                                    }
                                                }
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <div class="modal fade" id="addcontacto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><p align="center">Agregar Contacto</p></h4>
                  </div>
                  <div class="modal-body">
                    <form action="<?php print path("sisven/crear_contactos/") ?>" method="POST">
                        <select class="form-control" name="id_lista">
                            <option>Selecciona una Lista</option>
                            <?php
                                if(is_Array($listas)){
                                    for ($i=0; $i <= $listas[$i]["id"]; $i++) {
                                        echo "<option value=".$listas[$i]['id'].">".$listas[$i]['name']."</option>";
                                    }
                                }
                            ?>
                        </select>
                        <select class="form-control" name="id_usuario">
                            <option>Selecciona un Contacto</option>
                            <?php
                                if(is_Array($usuarios)){
                                    for ($i=0; $i <= $usuarios[$i]["id"]; $i++) {
                                       
                                        echo "<option value=".$usuarios[$i]['id'].">".$usuarios[$i]['nombre'].", ".$usuarios[$i]['apellido']."</option>";    
                                        
                                    }
                                }
                            ?>
                        </select>
                    
                  </div>
                 <div class="modal-footer">
                        <input type="submit" name="crear" value="Guardar" class="btn btn-success large "/>
                    </form>
                    <a title='Abrir Acciones' class='open-borrar btn btn-success bt-lg' href="#listas">
                        <i class="fa fa-table"></i>
                            Crear Listas
                    </a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <div class="modal fade" id="listas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><p align="center">Crear Listas de Contactos</p></h4>
                  </div>
                  <div class="modal-body">
                    <form action="<?php print path("sisven/crear_lista/") ?>" method="POST">
                        <input type="text" name="nombre"  placeholder="nombre lista" class="form-control" required autofocus/>
                        <input type="text" name="descripcion"  placeholder="Descripcion" class="form-control" required />
                        
                  </div>
                 <div class="modal-footer">
                        <input type="submit" name="guardar" value="Crear Lista" class="btn btn-success large "/>
                    </form>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <div class="modal fade" id="borrar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><p align="center">Borrar Contacto</p></h4>
                  </div>
                  <div class="modal-body">
                    <form action="<?php print path("sisven/archivos_guardar_status/") ?>" method="POST">
                        <input type="text" style="visibility: hidden" name="archivo_id" id="bookId" size="10"value="" />
                        <input type="submit" name="papelera" value="papelera" class="btn btn-danger large "/>
                    </form>
                  </div>
                 <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
               <script type="text/javascript">
                $(document).on("click", ".open-contacto", function (e) {

                    e.preventDefault();

                    var _self = $(this);

                    var myBookId = _self.data('id');
                    $("#bookId").val(myBookId);

                    $(_self.attr('href')).modal('show');
                });

                $(document).on("click", ".open-solicitudes", function (e) {

                    e.preventDefault();

                    var _self = $(this);

                    var myBookId = _self.data('id');
                    $("#bookId").val(myBookId);

                    $(_self.attr('href')).modal('show');
                });

                $(document).on("click", ".open-borrar", function (e) {

                    e.preventDefault();

                    var _self = $(this);

                    var myBookId = _self.data('id');
                    $("#bookId").val(myBookId);

                    $(_self.attr('href')).modal('show');
                });
            </script>