<div class="wrap">	
	<div class="site-header-container container">
	<div class="row">
		<div class="col-md-12">
			
			<header class="site-header">
			
				<section class="site-logo">
				
					<a href="#">
						<img src="<?php print $this->themePath; ?>/images/logo.png" width="120" />
					</a>
					
				</section>
				
				<nav class="site-nav">
					
					<ul class="main-menu hidden-xs" id="main-menu">
						<li >
							<a href="<?php print path("biblioteca/index/")?>">
								<span>Inicio</span>
							</a>
						</li>
						<li class="active">
							<a href="<?php print path("biblioteca/ver_catalogo/")?>">
								<span>Libros</span>
							</a>
						</li>
						<li>
							<a href="javascript:;" onclick="jQuery('#quienes_somos').modal('show', {backdrop: 'static'});" class="btn btn-default">
								<span>Quienes Somos</span>
							</a>
						</li>
						<li>
							<a href="<?php print path("biblioteca/login/")?>">
								<span>Login</span>
							</a>
						</li>
						<li>
							<a href="javascript:;" onclick="jQuery('#modal-5').modal('show', {backdrop: 'static'});" class="btn btn-default">
								<span>Contactanos</span>
							</a>
						</li>
						<li class="search">
							<a href="#">
								<i class="entypo-search"></i>
							</a>
							
							<form method="get" class="search-form" action="<?php print path("biblioteca/buscar_libro_espesifico/")?>" enctype="application/x-www-form-urlencoded">
								<input type="text" class="form-control" name="letra" placeholder="Buscar" />
							</form>
						</li>
					</ul>
					
				
					<div class="visible-xs">
						
						<a href="#" class="menu-trigger">
							<i class="entypo-menu"></i>
						</a>
						
					</div>
				</nav>
				
			</header>
			
		</div>
		
	</div>
	
</div>	
<section class="clients-logos-container slider-container" >
	<div class="container">
		<div class="row">
			<ol class="breadcrumb bc-3">
			    <li>
			        <a href="<?php print path("biblioteca/buscar_libro_por/?letra=A")?>">A</a>
			    </li>
			    <li>
			        <a href="<?php print path("biblioteca/buscar_libro_por/?letra=B")?>">B</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=C")?>">C</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=D")?>">D</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=E")?>">E</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=F")?>">F</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=G")?>">G</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=H")?>">H</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=I")?>">I</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=J")?>">J</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=K")?>">K</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=L")?>">L</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=M")?>">M</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=N")?>">N</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=O")?>">O</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=P")?>">P</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=Q")?>">Q</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=R")?>">R</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=S")?>">S</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=T")?>">T</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=U")?>">U</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=V")?>">V</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=W")?>">W</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=Y")?>">Y</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=Z")?>">Z</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=1")?>">1</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=2")?>">2</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=3")?>">3</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=4")?>">4</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=5")?>">5</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=6")?>">6</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=7")?>">7</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=8")?>">8</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=9")?>">9</a>
			    </li>
			    <li>
			    	<a href="<?php print path("biblioteca/buscar_libro_por/?letra=0")?>">0</a>
			    </li>
			</ol>
			<ul class="breadcrumb bc-3">
				<?php for ($i=0; $i <= $dewey[$i]["id"] ; $i++) { 
				?>
					<li><a href="<?php print path('biblioteca/buscar_dewey/?id='.$dewey[$i]["id"].'')?>"><?php print $dewey[$i]["nombre"];?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
</div>
<section class="portfolio-widget">
	
	<div class="container">
		
		<div class="row">
			
			<div class="col-sm-3">
				
				<div class="portfolio-info">
					<h3>
						<a href="#">Ultimos Libros</a>
					</h3>
					
					<p>Son los Ultimos Libros ingresados a la biblioteca</p>
				</div>
				
			</div>
	<?php 
	for($i=0; $i <= $ultimos_libros[$i]["id"]; $i++){
		for ($j=0; $j <= $dewey[$j]["id"] ; $j++) { 
            if($ultimos_libros[$i]["id_dewey"]==$dewey[$j]["id"]){
	?>
			<!-- begin 1-->
			<div class="col-sm-3">	
				<div class="portfolio-item">
					<a href="<?php print path('biblioteca/libro_datos/?id='.$ultimos_libros[$i][id].'') ?>" class="image">
						<img src="<?php print $this->themePath; ?>/images/<?php print $ultimos_libros[$i]["img_ruta"]; ?>" class="img-rounded" />
						<span class="hover-zoom"></span>
					</a>
					
					<h4>
						<a href="#" class="name"><?php print $ultimos_libros[$i]["nombre"]; ?></a>
					</h4>
					
					<div class="categories">
						<a href="#"><?php print $dewey[$i]["nombre"]; ?></a>
					</div>
				</div>
			</div>
			<!--end 1-->
	<?php } } }?>
		</div>
		
	</div>
	
</section>


	<section class="footer-widgets slider-container">
	
	<div class="container">
		
		<div class="row">
			
			<div class="col-sm-6">
				
				<a href="#">
					<img src="<?php print $this->themePath; ?>/images/otroo.png" width="100" />
				</a>
				
				<p>
					Biblioteca Publica. <br />
				</p>
				
			</div>
			
			<div class="col-sm-3">
				
				<h5>Direccion</h5>
				
				<p>
					<?php print $informacion_ubicacion[0]['nombre']; ?><br />
					<?php print $informacion_ubicacion[0]['direccion']; ?><br />
				</p>
				
			</div>
			
			<div class="col-sm-3">
				
				<h5>Contacto</h5>
				
				<p>
					Telefono: <?php print $informacion_ubicacion[0]['telefono']; ?> <br />
					Correo: <?php print $informacion_ubicacion[0]['correo']; ?>
				</p>
				
			</div>
			
		</div>
		
	</div>
	
</section>


<footer class="site-footer">

	<div class="container">
	
		<div class="row">
			
			<div class="col-sm-6">
				<!--Neon/Cepdech - Creative Commons 3.0 -BY -NoShared.--> 
			</div>
			
			<div class="col-sm-6">
				
				<ul class="social-networks text-right">
					<li>
						<a target="_blank" href="https://www.twitter.com/<?php print $informacion_ubicacion[0]['twitter']; ?>">
							<i class="entypo-twitter"></i>
						</a>
					</li>
					<li>
						<a href="#">
							<i class="entypo-facebook"></i>
						</a>
					</li>
				</ul>
				
			</div>
			
		</div>
		
	</div>
	
</footer>	
</div>
<div class="modal fade" id="modal-5">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Contactanos</h4>
			</div>
			
			<div class="modal-body">
			
				<form action="<?php print path("biblioteca/enviar_mensaje_interno"); ?>" method="post" role="form" class="form-horizontal" id="frmcontactanos">
					<h3 align="center">Contactanos</h3>		
					</br>
					<input class="form-control" name="nombres" type="text" placeholder="Nombre Completo" required="required">
					</br>
					<input class="form-control" name="email"  type="Email" placeholder="Correo Electronico" required="required">
					</br>
					<input class="form-control" name="telefono" type="phone" placeholder="Número Telefónico" required="required">			
					</br>
					<select class="form-control" name="estado">
		              <option>Elige un Estado</option>
		              <option>Amazonas</option>
		              <option>Anzoategui</option>
		              <option>Apure</option>
		              <option>Aragua</option>
		              <option>Barinas</option>
		              <option>Bolivar</option>
		              <option>Carabobo</option>
		              <option>Cojedes</option>
		              <option>Delta Amacuro</option>
		              <option>Distrito Capital</option>
		              <option>Falcon</option>
		              <option>Guarico </option>
		              <option>Lara</option>
		              <option>Merida</option>
		              <option>Monagas</option>
		              <option>Nueva Esparta</option>
		              <option>Portuguesa</option>
		              <option>Sucre</option>
		              <option>Tachira</option>
		              <option>Trujillo</option>
		              <option>Vargas</option>
		              <option>Yaracuy</option>
		              <option>Zulia</option>
		            </select>			
					</br>
					<textarea class="form-control" name="mensaje" placeholder="Deje su mensaje" required="required"></textarea>  
					</br>
					<input type="submit" class="btn btn-lg btn-primary btn-block large" name="enviar_btn" value="Enviar Mensaje" />               
				</form>
				
				
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="quienes_somos">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">¿Quienes Somos?</h4>
			</div>
			
			<div class="modal-body">
				<h3><?php print $publicidad[0]["nombre"]; ?></h3> <br />
				<?php print $publicidad[0]["descripcion"]; ?> <br /><br />
				<h3><?php print $mision[0]["nombre"]; ?></h3><br />
				<?php print $mision[0]["descripcion"]?> <br /> <br />
				<h3><?php print $vision[0]["nombre"]?></h3><br />
				<?php print $vision[0]["descripcion"]?> <br /> <br />
				
			</div>
			
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
