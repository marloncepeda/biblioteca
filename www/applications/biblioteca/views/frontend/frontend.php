<div class="wrap">	
	<div class="site-header-container container">
	<div class="row">
		<div class="col-md-12">
			
			<header class="site-header">
			
				<section class="site-logo">
				
					<a href="#">
						<img src="<?php print $this->themePath; ?>/images/logo.png" width="120" />
					</a>
					
				</section>
				
				<nav class="site-nav">
					
					<ul class="main-menu hidden-xs" id="main-menu">
						<li class="active">
							<a href="<?php print path("biblioteca/index/")?>">
								<span>Inicio</span>
							</a>
						</li>
						<li>
							<a href="<?php print path("biblioteca/ver_catalogo/")?>">
								<span>Libros</span>
							</a>
						</li>
						<li>
							<a href="javascript:;" onclick="jQuery('#quienes_somos').modal('show', {backdrop: 'static'});" class="btn btn-default">
								<span>Quienes Somos</span>
							</a>
						</li>
						<li>
							<a href="<?php print path("biblioteca/login/")?>">
								<span>Login</span>
							</a>
						</li>
						<li>
							<a href="javascript:;" onclick="jQuery('#modal-5').modal('show', {backdrop: 'static'});" class="btn btn-default">
								<span>Contactanos</span>
							</a>
						</li>
						<li class="search">
							<a href="#">
								<i class="entypo-search"></i>
							</a>
							
							<form method="get" class="search-form" action="<?php print path("biblioteca/buscar_libro_espesifico/")?>" enctype="application/x-www-form-urlencoded">
								<input type="text" class="form-control" name="letra" placeholder="Buscar" />
							</form>
						</li>
					</ul>
					
				
					<div class="visible-xs">
						
						<a href="#" class="menu-trigger">
							<i class="entypo-menu"></i>
						</a>
						
					</div>
				</nav>
				
			</header>
			
		</div>
		
	</div>
	
</div>	
<section class="slider-container" style="background-image: url(<?php print $this->themePath; ?>/frontend/images/slide-img-1-bg.png);">
	
	<div class="container">
		<div class="row">
			
			<div class="col-md-12">	
				<div class="slides">
					<div class="slide">
						
						<div class="slide-content">
							<h2>
								<small><?php print $ultimos_libros[0]["nombre"]; ?></small>
								Libros Disponbles: <?php print $ultimos_libros[0]["cantidad"]; ?>
							</h2>
							
							<p>
								<?php print $ultimos_libros[0]["descripcion"]; ?> <br />
								<?php print $ultimos_libros[0]["autor"]; ?>
							</p>
						</div>
						<div class="slide-image">	
							<a href="index.html#">
								<img src="<?php print $this->themePath; ?>/images/<?php print $ultimos_libros[0]["img_ruta"]; ?>" class="img-responsive" />
							</a>
						</div>
					</div>
					
					<div class="slide">

						<div class="slide-image">		
							<a href="#">
								<img src="<?php print $this->themePath; ?>/images/<?php print $ultimos_libros[1]["img_ruta"]; ?>" class="img-responsive" />
							</a>
						</div>
						<div class="slide-content text-right">
							<h2>
								<small><?php print $ultimos_libros[1]["nombre"]; ?></small>
								Libros Disponibles: <?php print $ultimos_libros[1]["cantidad"]; ?>
							</h2>	
							<p>
								<?php print $ultimos_libros[1]["descripcion"]; ?> <br />
								<?php print $ultimos_libros[1]["autor"]; ?><br />
								
							</p>
						</div>
					</div>
					
					<div class="slide">
					
						<div class="slide-content">
							<h2>
								<small><?php print $ultimos_libros[2]["nombre"]; ?></small>
								Libros Disponibles: <?php print $ultimos_libros[2]["cantidad"]; ?>
							</h2>		
							<p>
								<?php print $ultimos_libros[2]["descripcion"]; ?> <br />
								<?php print $ultimos_libros[2]["autor"]; ?>
							</p>
						</div>
						<div class="slide-image">
							<a href="#">
								<img src="<?php print $this->themePath; ?>/images/<?php print $ultimos_libros[2]["img_ruta"]; ?>" class="img-responsive" />
							</a>
						</div>
						
					</div>
					
					<div class="slides-nextprev-nav">
						<a href="#" class="prev">
							<i class="entypo-left-open-mini"></i>
						</a>
						<a href="#" class="next">
							<i class="entypo-right-open-mini"></i>
						</a>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</section>
<section class="features-blocks">
	
	<div class="container">
		
		<div class="row vspace">			
			<div class="col-sm-4">
				
				<div class="feature-block">
					<h3>
						<i class="entypo-cog"></i>
						<?php print $publicidad[1]["nombre"]; ?>
					</h3>
					
					<p>
						<?php print $publicidad[1]["descripcion"]; ?>
					</p>
				</div>
				
			</div>
			
			<div class="col-sm-4">
				
				<div class="feature-block">
					<h3>
						<i class="entypo-gauge"></i>
						<?php print $publicidad[2]["nombre"]; ?>
					</h3>
					
					<p>
						<?php print $publicidad[2]["descripcion"]; ?>
					</p>
				</div>
				
			</div>
			
			<div class="col-sm-4">
				
				<div class="feature-block">
					<h3>
						<i class="entypo-lifebuoy"></i>
						<?php print $publicidad[3]["nombre"]; ?>
					</h3>
					
					<p>
						<?php print $publicidad[3]["descripcion"]; ?>
					</p>
				</div>
				
			</div>
			
		</div>
		
				<div class="row">
			<div class="col-md-12">
				<hr />
			</div>
		</div>
		
	</div>
	
</section>
<section class="clients-logos-container slider-container" >
	
	<div class="container">
		
		<div class="row">
			<input type="text" style="visibility: hidden" name="libro_id" id="bookId" size="10"value="" />
			
			<div class="client-logos carousel slide" data-ride="carousel" data-interval="5000">
			
				<div class="carousel-inner">
				
					<div class="item active">
					
						<a href="#">
							<img height="20" width="100" src="<?php print $this->themePath; ?>/images/<?php print $mas_vistos_libros[0]["img_ruta"]; ?>" />
						</a>
						<a href="#">
							<img height="20" width="100" src="<?php print $this->themePath; ?>/images/<?php print $mas_vistos_libros[1]["img_ruta"]; ?>" />
						</a>
						<a href="#">
							<img height="20" width="100" src="<?php print $this->themePath; ?>/images/<?php print $mas_vistos_libros[2]["img_ruta"]; ?>" />
						</a>
						<a href="#">
							<img height="20" width="100" src="<?php print $this->themePath; ?>/images/<?php print $mas_vistos_libros[3]["img_ruta"]; ?>" />
						</a>
					</div>


					<div class="item">
					
						<a href="#">
							<img height="20" width="100" src="<?php print $this->themePath; ?>/images/<?php print $mas_vistos_libros[4]["img_ruta"]; ?>" />
						</a>
						<a href="#">
							<img height="20" width="100" src="<?php print $this->themePath; ?>/images/<?php print $mas_vistos_libros[5]["img_ruta"]; ?>" />
						</a>
						<a href="#">
							<img height="20" width="100" src="<?php print $this->themePath; ?>/images/<?php print $mas_vistos_libros[6]["img_ruta"]; ?>" />
						</a>
						<a href="#">
							<img height="20" width="100" src="<?php print $this->themePath; ?>/images/<?php print $mas_vistos_libros[7]["img_ruta"]; ?>" />
						</a>
						
					</div>
					
				</div>
				
			</div>
			<input type="text" style="visibility: hidden" name="libro_id" id="bookId" size="10"value="" />
			
		</div>
		
	</div>
	
</section>	
<section class="portfolio-widget">
	
	<div class="container">
		
		<div class="row">
			
			<div class="col-sm-3">
				
				<div class="portfolio-info">
					<h3>
						<a href="#">Ultimos Libros</a>
					</h3>
					
					<p>Son los Ultimos Libros ingresados a la biblioteca</p>
				</div>
				
			</div>
	<?php 
	for($i=0; $i <= $ultimos_libros[$i]["id"]; $i++){
		for ($j=0; $j <= $dewey[$j]["id"] ; $j++) { 
            if($ultimos_libros[$i]["id_dewey"]==$dewey[$j]["id"]){
	?>
			<!-- begin 1-->
			<div class="col-sm-3">	
				<div class="portfolio-item">
					<a href="#" class="image">
						<img src="<?php print $this->themePath; ?>/images/<?php print $ultimos_libros[$i]["img_ruta"]; ?>" class="img-rounded" />
						<span class="hover-zoom"></span>
					</a>
					
					<h4>
						<a href="#" class="name"><?php print $ultimos_libros[$i]["nombre"]; ?></a>
					</h4>
					
					<div class="categories">
						<a href="#"><?php print $dewey[$i]["nombre"]; ?></a>
					</div>
				</div>
			</div>
			<!--end 1-->
	<?php } } }?>
		</div>
		
	</div>
	
</section>

<!--<section class="testimonials-container">
	
	<div class="container">
		
		<div class="col-md-12">
			
			<div class="testimonials carousel slide" data-interval="8000">
			
				<div class="carousel-inner">
					
					<div class="item active">
					
						<blockquote>
							<p>
								Comentario del cliente.
							</p>
							<small>
								<cite>Cliente 1</cite> - Tipo de cliente
							</small>
						</blockquote>
						
					</div>
					
					<div class="item">
					
						<blockquote>
							<p>
								Comentario del cliente.
							</p>
							<small>
								<cite>Cliente 2</cite> - Tipo de cliente
							</small>
						</blockquote>
						
					</div>

					<div class="item">
					
						<blockquote>
							<p>
								Comentario del cliente.
							</p>
							<small>
								<cite>Cliente N</cite> - Tipo de cliente
							</small>
						</blockquote>
						
					</div>
				
				</div>
			
			</div>
			
		</div>
		
	</div>
	
</section>-->

	<section class="footer-widgets slider-container">
	
	<div class="container">
		
		<div class="row">
			
			<div class="col-sm-6">
				
				<a href="#">
					<img src="<?php print $this->themePath; ?>/images/otroo.png" width="100" />
				</a>
				
				<p>
					Biblioteca Publica. <br />
				</p>
				
			</div>
			
			<div class="col-sm-3">
				
				<h5>Direccion</h5>
				
				<p>
					<?php print $informacion_ubicacion[0]['nombre']; ?><br />
					<?php print $informacion_ubicacion[0]['direccion']; ?><br />
				</p>
				
			</div>
			
			<div class="col-sm-3">
				
				<h5>Contacto</h5>
				
				<p>
					Telefono: <?php print $informacion_ubicacion[0]['telefono']; ?> <br />
					Correo: <?php print $informacion_ubicacion[0]['correo']; ?>
				</p>
				
			</div>
			
		</div>
		
	</div>
	
</section>


<footer class="site-footer">

	<div class="container">
	
		<div class="row">
			
			<div class="col-sm-6">
				<!--Neon/Cepdech - Creative Commons 3.0 -BY -NoShared. -->
			</div>
			
			<div class="col-sm-6">
				
				<ul class="social-networks text-right">
					<li>
						<a target="_BLANK" href="https://twitter.com/bibliotecaPAEB">
							<i class="entypo-twitter"></i>
						</a>
					</li>
					<li>
						<a target="_BLANK" href="https://www.facebook.com/bibliotecaandreseb">
							<i class="entypo-facebook"></i>
						</a>
					</li>
				</ul>
				
			</div>
			
		</div>
		
	</div>
	
</footer>	
</div>
<div class="modal fade" id="modal-5">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Contactanos</h4>
			</div>
			
			<div class="modal-body">
			
			<form action="<?php print path("biblioteca/enviar_mensaje_interno"); ?>" method="post" role="form" class="form-horizontal" id="frmcontactanos">
					<h3 align="center">Contactanos</h3>		
					</br>
					<input class="form-control" name="nombres" type="text" placeholder="Nombre Completo" required="required">
					</br>
					<input class="form-control" name="email"  type="Email" placeholder="Correo Electronico" required="required">
					</br>
					<input class="form-control" name="telefono" type="phone" placeholder="Número Telefónico" required="required">			
					</br>
					<select class="form-control" name="estado">
		              <option>Elige un Estado</option>
		              <option>Amazonas</option>
		              <option>Anzoategui</option>
		              <option>Apure</option>
		              <option>Aragua</option>
		              <option>Barinas</option>
		              <option>Bolivar</option>
		              <option>Carabobo</option>
		              <option>Cojedes</option>
		              <option>Delta Amacuro</option>
		              <option>Distrito Capital</option>
		              <option>Falcon</option>
		              <option>Guarico </option>
		              <option>Lara</option>
		              <option>Merida</option>
		              <option>Monagas</option>
		              <option>Nueva Esparta</option>
		              <option>Portuguesa</option>
		              <option>Sucre</option>
		              <option>Tachira</option>
		              <option>Trujillo</option>
		              <option>Vargas</option>
		              <option>Yaracuy</option>
		              <option>Zulia</option>
		            </select>			
					</br>
					<textarea class="form-control" name="mensaje" placeholder="Deje su mensaje" required="required"></textarea>  
					</br>
					<input type="submit" class="btn btn-lg btn-primary btn-block large" name="enviar_btn" value="Enviar Mensaje" />               
				</form>
				
				
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="quienes_somos">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">¿Quienes Somos?</h4>
			</div>
			
			<div class="modal-body">
				<h3><?php print $publicidad[0]["nombre"]; ?></h3> <br />
				<?php print $publicidad[0]["descripcion"]; ?> <br /><br />
				<h3><?php print $mision[0]["nombre"]; ?></h3><br />
				<?php print $mision[0]["descripcion"]?> <br /> <br />
				<h3><?php print $vision[0]["nombre"]?></h3><br />
				<?php print $vision[0]["descripcion"]?> <br /> <br />
				
			</div>
			
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
