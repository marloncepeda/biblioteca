		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Crear Libro</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           <div class="container" >
              <div class="row">
                  <div class="col-md-6 col-md-offset-2">
                      <div class=" panel panel-default">
                          <div class="panel-body">
                              <form action="<?php print $href; ?>" method="post" enctype="multipart/form-data">
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" name="nombre_libro" placeholder="Nombres" type="text" autofocus required />
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Descripcion" name="descripcion" type="text" required/>
                                    </div>
                                     <div class="form-group">
                                        <input class="form-control" placeholder="Autor" name="autor" type="text" required/>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Cantidad de Libros" name="cnt_libros" type="text" required/>
                                    </div>
                                    <div class="form-group">
                                      <select class="form-control" name="dewey">
                                        <?php
                                          for ($i=0; $i <= $dewey[$i]["id"]; $i++) { 
                                            echo "<option value='".$dewey[$i]['id']."'>".$dewey[$i]['codigo']." - ".$dewey[$i]['nombre']."</option>";
                                          }                                          
                                        ?>
                                      </select>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" name="bookfile" type="file" required />
                                    </div>
                                     <div class="form-group">
                                        <input class="form-control" name="bookfile2" type="file" />
                                    </div>
                                     <div class="form-group">
                                        <input class="form-control" name="bookfile3" type="file" />
                                    </div>
                                     <div class="form-group">
                                        <input class="form-control" name="bookfile4" type="file" />
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Etiquetas" name="tags" type="text" required/>
                                    </div>
                                    <input class="btn btn-lg btn-success btn-block" name="registrar" type="submit" value="Crear">
                                </fieldset>
                              </form>
                          </div>
                      </div>

                  </div>
              </div>
            </div>