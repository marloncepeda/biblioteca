<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <?php if(is_Array($info_libro)){ ?>
                    <h1 class="page-header"><?php print $info_libro["0"]["nombre"]; ?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa entypo-book fa-fw"></i> Datos Del Libro 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <form action="<?php print path("bibliotecas/libros_modificar/") ?>" method="post">
                                <fieldset>
                                  <div class="form-group">
                                      <label>Nombre</label>
                                      <input class="form-control" name="nombre" type="text" value="<?php print $info_libro['0']['nombre']; ?>" required/>
                                  </div>
                                  <div class="form-group">
                                      <label>Descripcion</label>
                                      <input class="form-control" name="descripcion" pattern="[a-zA-Z]+" type="text" value="<?php print $info_libro['0']['descripcion']; ?>" readonly/>
                                  </div>
                                  <div class="form-group">
                                      <label>Autor</label>
                                      <input class="form-control" name="tags" pattern="[a-zA-Z]+" type="text" value="<?php print $info_libro['0']['autor']; ?>" readonly/>
                                  </div>
                                  <div class="form-group">
                                      <label>Numeracion</label>
                                      <input class="form-control" name="tags" pattern="[a-zA-Z]+" type="text" value="<?php 
                                          for ($i=0; $i <$deway[$i]['id'] ; $i++) { 
                                            if($deway[$i]['id']==$info_libro['0']['id_dewey']){
                                              print $deway['0']['codigo'].".".$info_libro['0']['id_dewey_decimal']; 
                                              $nombre =$deway['0']['nombre'];
                                            }
                                          }
                                        ?>" readonly/>
                                  </div>
                                  <div class="form-group">
                                      <label>Clasificacion</label>
                                      <input class="form-control" name="tags" pattern="[a-zA-Z]+" type="text" value="<?php print $nombre; ?>" readonly/>
                                  </div>
                                  <div class="form-group">
                                      <label>Cantidads</label>
                                      <input class="form-control" name="tags" pattern="[a-zA-Z]+" type="text" value="<?php print $info_libro['0']['cantidad']; ?>" readonly/>
                                  </div>
                                  <div class="form-group">
                                      <label>Etiquetas</label>
                                      <input class="form-control" name="tags" pattern="[a-zA-Z]+" type="text" value="<?php print $info_libro['0']['tags']; ?>" readonly/>
                                  </div>
                                  <div class="form-group">
                                    <label>Imagenes:</label>
                                    <a href="<?php print $this->themePath; ?>/images/<?php echo $info_libro['0']['img_ruta']; ?>" target="_BLANK" >
                                      <img src="<?php print $this->themePath; ?>/images/<?php echo $info_libro['0']['img_ruta']; ?>" width="100px" heigth="100px"></img>
                                    </a>
                                    <a href="<?php print $this->themePath; ?>/images/<?php echo $info_libro['0']['img_ruta2']; ?>" target="_BLANK" >
                                      <img src="<?php print $this->themePath; ?>/images/<?php echo $info_libro['0']['img_ruta2']; ?>" width="100px" heigth="100px"></img>
                                    </a>
                                    <a href="<?php print $this->themePath; ?>/images/<?php echo $info_libro['0']['img_ruta3']; ?>" target="_BLANK" >
                                      <img src="<?php print $this->themePath; ?>/images/<?php echo $info_libro['0']['img_ruta3']; ?>" width="100px" heigth="100px"></img>
                                    </a>
                                    <a href="<?php print $this->themePath; ?>/images/<?php echo $info_libro['0']['img_ruta4']; ?>" target="_BLANK" >
                                      <img src="<?php print $this->themePath; ?>/images/<?php echo $info_libro['0']['img_ruta4']; ?>" width="100px" heigth="100px"></img>
                                    </a>
                                  </div>
                                  
                                </fieldset>
                                <?php }?>
                            </form>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->
                
                    
                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>