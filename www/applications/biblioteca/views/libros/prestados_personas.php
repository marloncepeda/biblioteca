

<ol class="breadcrumb bc-3">
    <li>
        <a href="#"><i class="entypo-home"></i>Biblioteca</a>
    </li>
    <li>
        <a href="#">Inventario</a>
    </li>
    <li class="active">
        <strong>Prestados a Personas</strong>
    </li>
</ol>
            
<h2>Todos los Usuarios con Libros Prestados</h2>

<br />

<table class="table table-bordered datatable" id="table-1">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>ID Libro</th>
            <th>Estado</th>
            <th>Fecha de Entrega</th>
           <th>Acciones</th>
    </thead>
    <tbody>
    <?php
        for($i=0; $i <= $prestamos[$i]["id"]; $i++){
            for ($j=0; $j <= $libros[$j]["id"] ; $j++) { 
                if($prestamos[$i]["id_user"]==$libros[$j]["id"]){
                    echo "<tr >";
                    echo "<td>".$libros[$j]["nombre"]."</td>";
                    echo "<td>".$libros[$j]["apellido"]."</td>";
                    echo "<td>".$prestamos[$i]["id_libro"]."</td>";
                    echo "<td>".$prestamos[$i]["status"]."</td>";
                    echo "<td>".$prestamos[$i]["fecha_prestamo_retorno"]."</td>";
                    echo "<td>"; ?>
                        <?php if($prestamos[$i]["status"]=="Reservado"){
                            echo "<a href='".path("biblioteca/libro_update_estado/?prestamo=".$prestamos[$i]["id"]."")."' class='btn btn-default btn-sm btn-icon icon-left'>
                            <i class='entypo-pencil'></i>
                            Entregar Reserva
                            </a> ";
                        }else{ 
                        echo "
                        <a data-id='".$prestamos[$i]["id"]."' data-status='".$prestamos[$i]["status"]."' data-id_libro='".$prestamos[$i]["id_libro"]."' data-nombre='".$libros[$j]["nombre"]."' href='#editar' class='open-edit btn btn-default btn-sm btn-icon icon-left'>
                            <i class='entypo-pencil'></i>
                            Entregar
                        </a> ";
                         }
                        echo "<a data-id='".$libros[$j]["id"]."' href='#borrar' class='open-borrar btn btn-danger btn-sm btn-icon icon-left'>
                            <i class='entypo-cancel'></i>
                            Borrar
                        </a>
                        
                        <a href='http://".get('IPgateway').":9090/sendsms?phone=".$libros[$j]['telefono']."&text=Recuerde devolver el libro&password=' target=_blank' class='btn btn-info btn-sm btn-icon icon-left'>
                            <i class='entypo-info'></i>
                            Enviar SMS
                        </a>
                    </td>";
                    echo "</tr>";
                    /* "<a data-id='".$libros[$j]["id"]."' href='#informacion_libro' class='open-info btn btn-info btn-sm btn-icon icon-left'>
                            <i class='entypo-info'></i>
                            Enviar Correo
                        </a>"*/
                }
            }
        }
    ?>
    </tbody>
</table>
</div></div>

<div class="modal fade" id="editar">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Entregar Libro</h4>
            </div>
            
            <div class="modal-body">
            
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        
                        <div class="form-group">
                            <form action="<?php print path("biblioteca/libros_entregar/") ?>" method="post">
                                <fieldset>
                                    <input type="text" style="visibility: hidden" name="prestamo_id" id="bookId" size="10"value="" />
                                    <input type="text" style="visibility: hidden" name="libro_id" id="bookdescription" size="10"value="" />
                                    <div class="form-group">
                                        <input class="form-control" name="name" id="bookname" required/>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" name="tags" type="text" id="booktags" required/>
                                    </div>
                                    <input class="btn btn-lg btn-success btn-block" name="entregar" type="submit" value="Entregar"/>                     
                                </fieldset>
                            </form>
                            
                        </div>  
                        
                    </div>
                </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

</div></div>
<div class="modal fade" id="informacion_libro">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Informacion Libro</h4>
            </div>
            
            <div class="modal-body">
            
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        
                        <div class="form-group">
                            <p>¿Quiere ver La informacion de este libro?</p>
                            
                        </div>  
                        
                    </div>
                </div>
            
            
            <div class="modal-footer">
                <form action="<?php print path("biblioteca/libros_perfil/") ?>" method="post">
                    <input type="text" style="visibility: hidden" name="libro_id" id="Idlibro" size="10"value="" />
                    <input type="submit" class="btn btn-default" name="borrar" value="Aceptar"/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </form>
            </div>
        </div>
    </div>
</div>

</div></div>
<div class="modal fade" id="borrar">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Borrar libro</h4>
            </div>
            
            <div class="modal-body">
            
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        
                        <div class="form-group">
                            <p align="center">¿Quieres Borrar el libro? una vez Borrado no aparecera mas en el inventario</p>
                            
                        </div>  
            
                    </div>
                </div>
    
            <div class="modal-footer">
                <form action="<?php print path("biblioteca/libros_borrar/") ?>" method="post">
                    <input type="text" style="visibility: hidden" name="libro_id" id="Id" size="10"value="" />
                    <input type="submit" class="btn btn-default" name="borrar" value="Aceptar"/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function($)
    {
        $("#table-1").dataTable({
            "sPaginationType": "bootstrap",
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bStateSave": true
        });
        
        
    });

    jQuery(document).on("click", ".open-edit", function (e) {
        e.preventDefault();
        var _self = $(this);
        var myBookId = _self.data('id');
        var myBookname = _self.data('nombre');
        var myBookidprestamolibro = _self.data('id_libro');
        var myBookstatus = _self.data('status');
        
        $("#bookId").val(myBookId);
        $("#bookname").val(myBookname);
        $("#bookdescription").val(myBookidprestamolibro);
        $("#booktags").val(myBookstatus);

        $(_self.attr('href')).modal('show',{backdrop: 'static'});
        //alert(myBookId1);
    });

    jQuery(document).on("click", ".open-info", function (e) {
        e.preventDefault();
        var _self = $(this);
        var myBookId = _self.data('id');
        $("#Idlibro").val(myBookId);
        $(_self.attr('href')).modal('show',{backdrop: 'static'});
        //alert(myBookId);
    });

    jQuery(document).on("click", ".open-borrar", function (e) {
        e.preventDefault();
        var _self = $(this);
        var myBookId = _self.data('id');
        $("#Id").val(myBookId);
        $(_self.attr('href')).modal('show',{backdrop: 'static'});
        //alert(myBookId);
    });
</script>