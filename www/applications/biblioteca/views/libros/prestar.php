		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                  <?php if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="bibliotecario")){ ?>
                    <h1 class="page-header">Prestar Libro</h1>
                  <?php }else{ ?>
                    <h1 class="page-header">Reservar Libro</h1>
                  <?php }?>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           <div class="container" >
              <div class="row">
                  <div class="col-md-6 col-md-offset-2">
                      <div class=" panel panel-default">
                          <div class="panel-body">
                            <?php if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="bibliotecario")){ ?>
                              <form action="<?php print path("biblioteca/libros_prestar"); ?>" method="post" enctype="multipart/form-data">
                            <?php }else{ ?>
                              <form action="<?php print path("biblioteca/libros_reservar"); ?>" method="post" enctype="multipart/form-data">
                            <?php } ?>
                                <fieldset>
                                   
                                   <div class="form-group">
                                      <select id="libro" class="col-md-12" name="libro_prestar">
                                        <option>Selecciona Un Libro</option>
                                         <?php
                                          for ($i=0; $i <= $libros[$i]["id"]; $i++) { 
                                            echo "<option value='".$libros[$i]['id']."'>Disponible: ".$libros[$i]['cantidad']." - Nombre: ".$libros[$i]['nombre']." - autor: ".$libros[$i]['autor']."</option>";
                                          }                                          
                                        ?>
                                      </select>
                                    </div>
                                   
                                  <?php if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="bibliotecario")){ ?>  
                                    
                                    <div> 
                                      <select class="col-md-12" name="persona_prestar" id="usuario">
                                        <option>Selecciona Una persona</option>
                                         <?php
                                          for ($i=0; $i <= $usuarios[$i]["id"]; $i++) { 
                                            echo "<option value='".$usuarios[$i]['id']."'>".$usuarios[$i]['nombre']." - ".$usuarios[$i]['apellido']."</option>";
                                          }                                          
                                        ?>
                                      </select>
                                    </div>
                                    <br />
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Cantidad dias a prestar" name="cant_dias" type="text"/>
                                    </div>
                                    
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Fecha de Retorno" name="fecha_retorno" type="date"/>
                                    </div>
                                    
                                    <input class="btn btn-lg btn-success btn-block" name="registrar" type="submit" value="Prestar Libro">
                                    
                                    <?php }else{ ?>
                                      <input class="btn btn-lg btn-success btn-block" name="registrar" type="submit" value="Reservar Libro">
                                    <?php } ?>

                                </fieldset>
                              </form>
                          </div>
                      </div>

                  </div>
              </div>
            </div>
          <script>
            $(document).ready(function() { $("#libro").select2(); });
             $(document).ready(function() { $("#usuario").select2(); });
          </script>