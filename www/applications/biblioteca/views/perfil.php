 <div id="page-wrapper">
   
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="entypo-user fa-fw"></i> Datos Personales |<a><i class="entypo-pencil"></i></a>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <form action="<?php print $href; ?>" method="post" enctype="multipart/form-data">
                                <fieldset>
                                  <div class="form-group">
                                      <input class="form-control" name="ci" type="text" value="<?php print SESSION("ci"); ?>" required/>
                                  </div>
                                  <div class="form-group">
                                      <input class="form-control" name="userfile" type="file" accept="image/jpeg">
                                  </div>
                                  <div class="form-group">
                                      <input class="form-control" name="nombres" pattern="[a-zA-Z]+" type="text" value="<?php print SESSION("nombre"); ?>" required/>
                                  </div>
                                   <div class="form-group">
                                      <input class="form-control" name="apellidos" pattern="[a-zA-Z]+" type="text" value="<?php print SESSION("apellido"); ?>" required/>
                                  </div>
                                   <div class="form-group">
                                      <input class="form-control" name="telefono" pattern="[0-9--]+" type="text" value="<?php print SESSION("telefono"); ?>" required/>
                                  </div>
                                   <div class="form-group">
                                      <input class="form-control" name="fecha" type="text" value="<?php print SESSION("fecha_user"); ?>" required/>
                                  </div>
                                   <div class="form-group">
                                      <input class="form-control" name="email" type="email" value="<?php print SESSION("correo"); ?>" required/>
                                  </div>
                                  <input class="btn btn-lg btn-success btn-block" name="modificar" type="submit" value="Modificar"/>                     
                                </fieldset>
                            </form>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bell fa-fw"></i> Ultimas Acciones
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="list-group">
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-flag fa-fw"></i> Ver perfil
                                    <span class="pull-right text-muted small"><em>1 minutos atras</em>
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-flag fa-fw"></i> Favoritos
                                    <span class="pull-right text-muted small"><em>5 minutos atras</em>
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-flag fa-fw"></i> Borrar Doc.
                                    <span class="pull-right text-muted small"><em>8 minutos atras</em>
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-flag fa-fw"></i> Entrar
                                    <span class="pull-right text-muted small"><em>12 minutos atras</em>
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-flag fa-fw"></i> Salir
                                    <span class="pull-right text-muted small"><em>2 dias atras</em>
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-flag fa-fw"></i> Entrar
                                    <span class="pull-right text-muted small"><em>2 dias atras</em>
                                    </span>
                                </a>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    <!--
                        Donut bar pendint
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Donut Chart Example
                        </div>
                        <div class="panel-body">
                            <div id="morris-donut-chart"></div>
                            <a href="#" class="btn btn-default btn-block">View Details</a>
                        </div>
                        <!-- /.panel-body --
                    </div>-->
                    
                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>