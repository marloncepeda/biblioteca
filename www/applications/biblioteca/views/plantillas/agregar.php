		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Crear Plantilla</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           <div class="container" >
              <div class="row">
                  <div class="col-md-6 col-md-offset-2">
                      <div class=" panel panel-default">
                          <div class="panel-body">
                              <form action="<?php print path("biblioteca/plantillas_agregar"); ?>" method="post">
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" name="nombre_plantilla" placeholder="Nombre " type="text" autofocus/>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Descripcion" name="descripcion" type="text"/>
                                    </div>
                                    <div class="form-group">
                                      <select class="form-control" name="tipo">
                                        <option>Seleccione un tipo de Plantilla</option>
                                        <option>SMS</option>
                                        <option>Correo</option>
                                      </select>
                                    </div>
                                    <input class="btn btn-lg btn-success btn-block" name="registrar" type="submit" value="Crear">
                                </fieldset>
                              </form>
                          </div>
                      </div>

                  </div>
              </div>
            </div>