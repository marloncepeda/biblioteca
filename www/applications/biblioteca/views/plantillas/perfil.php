 <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <?php if(is_Array($info_libro)){ ?>
                    <h1 class="page-header"><?php print $info_libro["0"]["nombre"]; ?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa entypo-book fa-fw"></i> Datos Del libro 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <form action="<?php print path("sisven/libros_modificar/") ?>" method="post">
                                <fieldset>
                                  <div class="form-group">
                                      <input type="text" style="visibility: hidden" name="libro_id" size="10" value="<?php print POST('libro_id'); ?>" />
                                      <input class="form-control" name="nombre" type="text" value="<?php print $info_libro['0']['nombre']; ?>" required/>
                                  </div>
                                  <div class="form-group">
                                      <input class="form-control" name="descripcion" pattern="[a-zA-Z]+" type="text" value="<?php print $info_libro['0']['descripcion']; ?>" readonly/>
                                  </div>
                                   <div class="form-group">
                                      <input class="form-control" name="tags" pattern="[a-zA-Z]+" type="text" value="<?php print $info_libro['0']['tags']; ?>" readonly/>
                                  </div>
                                  <input class="btn btn-lg btn-success btn-block" name="modificar" type="submit" value="Modificar"/>                     
                                </fieldset>
                                <?php }?>
                            </form>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->
                
                    
                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <div class="modal fade" id="addBookDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><p align="center">Adjuntar libro</p></h4>
                  </div>
                  <div class="modal-body">
                    <form enctype="multipart/form-data" action="<?php print $href; ?>" method="post">
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" name="userfile" type="file" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" placeholder="Nombres" name="descripcion" type="text" autofocus>
                                        </textarea>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Etiquetas" name="tags" type="text" />
                                    </div>
                                    <input class="btn btn-lg btn-success btn-block" name="subir" type="submit" value="subir">
                                </fieldset>
                              </form>
                  </div>
                 <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

              <script type="text/javascript">
                $(document).on("click", ".open-AddBookDialog", function (e) {

                    e.preventDefault();

                    var _self = $(this);

                    var myBookId = _self.data('id');
                    $("#bookId").val(myBookId);
                    
                    $(_self.attr('href')).modal('show');
                });
            </script>