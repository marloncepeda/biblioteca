

<ol class="breadcrumb bc-3">
    <li>
        <a href="#"><i class="entypo-home"></i>Biblioteca</a>
    </li>
    <li>
        <a href="#">Inventario</a>
    </li>
    <li class="active">
        <strong>Todos las Plantillas</strong>
    </li>
</ol>
            
<h2>Todos las Plantillas en Sistema</h2>

<br />

<table class="table table-bordered datatable" id="table-1">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Tipo</th>
            <th>Descripcion</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
    <?php
        for($i=0; $i <= $Plantillas[$i]["id"]; $i++){ 
            echo "<tr >";
            echo "<td>".$Plantillas[$i]["nombre"]."</td>";
            echo "<td>".$Plantillas[$i]["tipo"]."</td>";
            echo "<td>".$Plantillas[$i]["descripcion"]."</td>";
            echo "<td>
                <a data-id='".$Plantillas[$i]["id"]."' data-descripcion='".$Plantillas[$i]["descripcion"]."' data-nombre='".$Plantillas[$i]["nombre"]."' data-tipo='".$Plantillas[$i]["tipo"]."' href='#editar' class='open-edit btn btn-default btn-sm btn-icon icon-left'>
                    <i class='entypo-pencil'></i>
                    Editar
                </a>
                
                <a data-id='".$Plantillas[$i]["id"]."' href='#borrar' class='open-borrar btn btn-danger btn-sm btn-icon icon-left'>
                    <i class='entypo-cancel'></i>
                    Borrar
                </a>
    
            </td>";
            echo "</tr>";      
        }
    ?>
    </tbody>
</table>
</div></div>

<div class="modal fade" id="editar">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Editar Plantilla</h4>
            </div>
            
            <div class="modal-body">
            
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        
                        <div class="form-group">
                            <form action="<?php print path("biblioteca/plantillas_modificar/") ?>" method="post">
                                <fieldset>
                                    <input type="text" style="visibility: hidden" name="plantilla_id" id="bookId" size="10"value="" />
                                    <div class="form-group">
                                        <input class="form-control" name="name" id="bookname" required/>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" name="descripcion" id="bookdescription"type="text"  required/>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" name="tags" type="text" id="booktags" required/>
                                    </div>
                                    <input class="btn btn-lg btn-success btn-block" name="modificar" type="submit" value="Modificar"/>                     
                                </fieldset>
                            </form>
                            
                        </div>  
                        
                    </div>
                </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

</div></div>
<div class="modal fade" id="informacion_libro">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Informacion Plantilla</h4>
            </div>
            
            <div class="modal-body">
            
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        
                        <div class="form-group">
                            <p>¿Quiere ver La informacion de este Plantilla?</p>
                            
                        </div>  
                        
                    </div>
                </div>
            
            
            <div class="modal-footer">
                <form action="<?php print path("biblioteca/Plantillas_perfil/") ?>" method="post">
                    <input type="text" style="visibility: hidden" name="plantilla_id" id="Idlibro" size="10"value="" />
                    <input type="submit" class="btn btn-default" name="borrar" value="Aceptar"/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </form>
            </div>
        </div>
    </div>
</div>

</div></div>
<div class="modal fade" id="borrar">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Borrar Plantilla</h4>
            </div>
            
            <div class="modal-body">
            
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        
                        <div class="form-group">
                            <p align="center">¿Quieres Borrar la Plantilla? una vez Borrado no aparecera mas en el Sistema</p>
                            
                        </div>  
            
                    </div>
                </div>
    
            <div class="modal-footer">
                <form action="<?php print path("biblioteca/Plantillas_borrar/") ?>" method="post">
                    <input type="text" style="visibility: hidden" name="plantilla_id" id="Id" size="10"value="" />
                    <input type="submit" class="btn btn-default" name="borrar" value="Aceptar"/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function($)
    {
        $("#table-1").dataTable({
            "sPaginationType": "bootstrap",
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bStateSave": true
        });
        
        
    });

    jQuery(document).on("click", ".open-edit", function (e) {
        e.preventDefault();
        var _self = $(this);
        var myBookId = _self.data('id');
        var myBookname = _self.data('nombre');
        var myBookdescription = _self.data('descripcion');
        var myBooktags = _self.data('tipo');
        
        $("#bookId").val(myBookId);
        $("#bookname").val(myBookname);
        $("#bookdescription").val(myBookdescription);
        $("#booktags").val(myBooktags);

        $(_self.attr('href')).modal('show',{backdrop: 'static'});
        //alert(myBookId1);
    });

    jQuery(document).on("click", ".open-info", function (e) {
        e.preventDefault();
        var _self = $(this);
        var myBookId = _self.data('id');
        $("#Idlibro").val(myBookId);
        $(_self.attr('href')).modal('show',{backdrop: 'static'});
        //alert(myBookId);
    });

    jQuery(document).on("click", ".open-borrar", function (e) {
        e.preventDefault();
        var _self = $(this);
        var myBookId = _self.data('id');
        $("#Id").val(myBookId);
        $(_self.attr('href')).modal('show',{backdrop: 'static'});
        //alert(myBookId);
    });
</script>