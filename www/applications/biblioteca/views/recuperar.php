<body class="page-body login-page login-form-fall">
<div class="container" >
  <div class="row">
    <br />
    <br />
      <div class="col-md-6 col-md-offset-3">
          <div class="login-panel panel panel-default">
              <div class="panel-heading">
                  <h3 class="panel-title">Entrar</h3>
              </div>
              <div class="panel-body">
                  <form action="<?php print path("biblioteca/recuperar_clave"); ?>" method="post">
                    <fieldset>
                     <div class="form-group">
                          <input class="form-control" placeholder="Correo Electronico" name="correo" type="text" required/>
                      </div>
                      <div class="form-group">
                          <input class="form-control" placeholder="Pregunta de seguridad" name="pregunta" type="text" required/>
                      </div>
                      <div class="form-group">
                          <input class="form-control" placeholder="Respuesta de Seguridad" name="respuesta" type="text" required/>
                      </div>
                      <div class="form-group">
                          <input class="form-control" placeholder="Password" name="clave" type="password" required/>
                      </div>
                      <input class="btn btn-lg btn-primary btn-block" name="registro" type="submit" value="Registrar"/>
                      <a class="btn btn-lg btn-danger btn-block" href="<?php print path("biblioteca/login/"); ?>">Cancelar<a/>                      
                    </fieldset>
                  </form>
              </div>
          </div>

      </div>
  </div>
</div>