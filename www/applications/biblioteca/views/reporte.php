		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Elegir Clasificación</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           <div class="container" >
              <div class="row">
                  <div class="col-md-6 col-md-offset-2">
                      <div class=" panel panel-default">
                          <div class="panel-body">
                              <form action="<?php print path("biblioteca/reporte_libro_clasificacion"); ?>" method="post" >
                                    <div class="form-group">
                                      <select class="form-control" name="dewey">
                                        <?php
                                          for ($i=0; $i <= $dewey[$i]["id"]; $i++) { 
                                            echo "<option value='".$dewey[$i]['id']."'>".$dewey[$i]['codigo']." - ".$dewey[$i]['nombre']."</option>";
                                          }                                          
                                        ?>
                                      </select>
                                    </div>
                                    <input class="btn btn-lg btn-success btn-block" name="buscar" type="submit" value="Buscar">
                                </fieldset>
                              </form>
                          </div>
                      </div>

                  </div>
              </div>
            </div>