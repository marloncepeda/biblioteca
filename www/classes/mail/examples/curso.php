<?php

//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
date_default_timezone_set('Etc/UTC');

require '../PHPMailerAutoload.php';
$email=$_GET["email"];
$nombres=$_GET["nombres"];
//Create a new PHPMailer instance
$mail = new PHPMailer();

//Tell PHPMailer to use SMTP
$mail->isSMTP();

//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 2;

//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';

//Set the hostname of the mail server
$mail->Host = 'smtp.gmail.com';

//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 465;

//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'ssl';

//Whether to use SMTP authentication
$mail->SMTPAuth = true;

//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = "cepdechacademia@gmail.com";

//Password to use for SMTP authentication
$mail->Password = "yohanna1451marlo";

//Set who the message is to be sent from
$mail->setFrom('cepdechacademia@gmail.com', 'Cepdech Academia');

//Set an alternative reply-to address
$mail->addReplyTo('cepdechacademia@gmail.com', 'Cepdech Academia');

//Set who the message is to be sent to
$mail->addAddress($email, $nombres);

//Set the subject line
$mail->Subject = 'Registro de '.$_GET["tipo"].' en Cepdech - Academia';

//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
if($_GET["tipo"]="maya 3d"){
	$mail->msgHTML(file_get_contents('maya.html'), dirname(__FILE__));
	$mail->addAttachment('../images/maya_aviso.png');	
}elseif($_GET["tipo"]=="python basico"){
	$mail->msgHTML(file_get_contents('python.html'), dirname(__FILE__));
	$mail->addAttachment('images/python_aviso.png');
}elseif($_GET["tipo"]=="C++ Basico"){
	$mail->msgHTML(file_get_contents('cpp.html'), dirname(__FILE__));
	$mail->addAttachment('images/cpp_aviso.png');
}elseif($_GET["tipo"]=="Visual Basic"){
	$mail->msgHTML(file_get_contents('VB.html'), dirname(__FILE__));
	$mail->addAttachment('images/VBasic_aviso.png');
}elseif($_GET["tipo"]=="EnchantJS"){
	$mail->msgHTML(file_get_contents('enchantjs.html'), dirname(__FILE__));
	$mail->addAttachment('images/enchans_aviso.png');
}elseif($_GET["tipo"]=="nodejs Basico"){
	$mail->msgHTML(file_get_contents('nodejs.html'), dirname(__FILE__));
	$mail->addAttachment('images/nodejs_aviso.png');
}elseif($_GET["tipe"]=="blender"){
	$mail->msgHTML(file_get_contents('blender.html'), dirname(__FILE__));
	$mail->addAttachment('images/blender_aviso.png');
}
//send the message, check for errors
if (!$mail->send()) {
    //echo "Mailer Error: " . $mail->ErrorInfo;
} else {
    echo "<script>document.location.href='http://localhost/academia/';</script>";
}
?>
</body>
</html>
