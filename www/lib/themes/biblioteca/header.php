<?php 
    if(!defined("_access")) {
        die("Error: You don't have permission to access here..."); 
    }
?>
<!DOCTYPE html>
<html lang="<?php print get("webLang"); ?>">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title><?php print $this->getTitle(); ?></title>

        <link rel="stylesheet" href="<?php print $this->themePath; ?>/css/font-icons/entypo/css/entypo.css"  >
        <link rel="stylesheet" href="<?php print $this->themePath; ?>/css/font-icons/entypo/css/animation.css"  >
        
        <script src="<?php print $this->themePath; ?>/js/jquery.js" id="script-resource-0"></script>

        <?php if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="bibliotecario")){ ?>
            <link rel="stylesheet" href="<?php print $this->themePath; ?>/css/neon.css">
            <link rel="stylesheet" href="<?php print $this->themePath; ?>/css/jquery-ui-1.10.3.custom.min.css"  >
            <link rel="stylesheet" href="<?php print $this->themePath; ?>/css/custom.css">
            <link rel="stylesheet" href="<?php print $this->themePath; ?>/js/jvectormap/jquery-jvectormap-1.2.2.css"  >
            <link rel="stylesheet" href="<?php print $this->themePath; ?>/js/rickshaw/rickshaw.min.css"  >
            <link rel="stylesheet" href="<?php print $this->themePath; ?>/js/select2/select2.css">
            
            <script src="<?php print $this->themePath; ?>/js/gsap/main-gsap.js" ></script>
            <script src="<?php print $this->themePath; ?>/js/bootstrap.min.js" ></script>
            <script src="<?php print $this->themePath; ?>/js/joinable.js"></script>
            <script src="<?php print $this->themePath; ?>/js/resizeable.js"></script>
            <script src="<?php print $this->themePath; ?>/js/neon-custom.js"></script>
            <script src="<?php print $this->themePath; ?>/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js" ></script>
            <script src="<?php print $this->themePath; ?>/js/neon-api.js" ></script>
            <script src="<?php print $this->themePath; ?>/js/jvectormap/jquery-jvectormap-1.2.2.min.js" ></script>
            <script src="<?php print $this->themePath; ?>/js/jvectormap/jquery-jvectormap-europe-merc-en.js" ></script>
            <script src="<?php print $this->themePath; ?>/js/jquery.sparkline.min.js" ></script>
            <script src="<?php print $this->themePath; ?>/js/rickshaw/vendor/d3.v3.js" ></script>
            <script src="<?php print $this->themePath; ?>/js/rickshaw/rickshaw.min.js" ></script>
            <script src="<?php print $this->themePath; ?>/js/raphael-min.js" ></script>
            <script src="<?php print $this->themePath; ?>/js/morris.min.js" ></script>
            <script src="<?php print $this->themePath; ?>/js/neon-demo.js" ></script>
            <script src="<?php print $this->themePath; ?>/js/jquery.validate.min.js" ></script>
            <script src="<?php print $this->themePath; ?>/js/neon-login.js" ></script>
            <script src="<?php print $this->themePath; ?>/js/jquery.dataTables.min.js"></script>
            <script src="<?php print $this->themePath; ?>/js/dataTables.bootstrap.js"></script>
            <script src="<?php print $this->themePath; ?>/js/select2/select2.js" ></script>
            <script type="text/javascript">
                $(document).ready(function(){
                    $(".sidebar-collapse-icon").trigger("click");
                });
            </script>
        <?php }else{ ?>

            <link rel="stylesheet" href="<?php print $this->themePath; ?>/css/custom.css">
            <link rel="stylesheet" href="<?php print $this->themePath; ?>/css/neon.css">        
            <link rel="stylesheet" href="<?php print $this->themePath; ?>/frontend/js/nivo-lightbox/nivo-lightbox.css" id="style-resource-4">
            <link rel="stylesheet" href="<?php print $this->themePath; ?>/frontend/js/nivo-lightbox/themes/default/default.css" id="style-resource-5">



            <link rel="stylesheet" href="<?php print $this->themePath; ?>/frontend/css/neon.css">
            <link rel="stylesheet" href="<?php print $this->themePath; ?>/frontend/css/bootstrap-min.css"  >
            <script src="<?php print $this->themePath; ?>/frontend/js/bootstrap.js" ></script>
            <script src="<?php print $this->themePath; ?>/frontend/js/joinable.js" ></script>
            <script src="<?php print $this->themePath; ?>/frontend/js/jquery.cycle2.min.js" ></script>
            <script src="<?php print $this->themePath; ?>/frontend/js/neon-custom.js" ></script>
            <script src="<?php print $this->themePath; ?>/frontend/js/neon-slider.js" ></script>
            <script src="<?php print $this->themePath; ?>/frontend/js/resizeable.js" ></script>
            <script src="<?php print $this->themePath; ?>/frontend/js/gsap/main-gsap.js" ></script>
            <script src="<?php print $this->themePath; ?>/frontend/js/isotope/jquery.isotope.min.js" ></script>
            <script src="<?php print $this->themePath; ?>/frontend/js/neon-slider.js" ></script>
            
            <script src="<?php print $this->themePath; ?>/js/neon-custom.js"></script>
            <script src="<?php print $this->themePath; ?>/js/neon-api.js" ></script>
            <script src="<?php print $this->themePath; ?>/js/neon-demo.js" ></script>
            <script src="<?php print $this->themePath; ?>/js/jquery.validate.min.js" ></script>
            <script src="<?php print $this->themePath; ?>/js/neon-login.js" ></script>
            <script type="text/javascript">
                // Setup Gallery Lightbox
                jQuery(document).ready(function($)
                {
                    $(".gallery-item .image").nivoLightbox();
                });
                </script>

        <?php }?>
        <style type="text/css">
            body{
                background: url('<?php print $this->themePath; ?>/images/fondo.jpg');
            }
            .page-container .main-content{
                background: url('<?php print $this->themePath; ?>/images/fondo.jpg');
            }
        </style>


    </head>
<?php if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="bibliotecario")){ ?>
    <body>
        <div class="page-container">    
            <div class="sidebar-menu">
                <header class="logo-env">

                    <!-- logo -->
                    <div class="logo">
                        <a href="<?php print path("biblioteca/perfil_usuario/")?>">
                            <img src="<?php print $this->themePath; ?>/images/<?php print SESSION('ruta_img'); ?>" width="120" height="120" alt="<?php print SESSION('nombre').' '.SESSION('apellido'); ?>" />
                        </a>
                    </div>
                    
                    <!-- logo collapse icon -->            
                    <div class="sidebar-collapse">
                        <a href="i#" class="sidebar-collapse-icon with-animation"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                            <i class="entypo-menu"></i>
                        </a>
                    </div>
                    
                    <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                    <div class="sidebar-mobile-menu visible-xs">
                        <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                            <i class="entypo-menu"></i>
                        </a>
                    </div>
                    
                </header>
                
                <ul id="main-menu" class="">
                    <li id="search">
                        <form method="POST" action="<?php print path("biblioteca/libros_buscar/") ?>">
                            <input type="text" name="buscar" class="search-input" placeholder="Buscar..."/>
                            <button type="submit">
                                <i class="entypo-search"></i>
                            </button>
                        </form>
                    </li>
                    <li><!--class="opened active"-->
                        <a>
                            <i class="entypo-book-open"></i>
                            <span>Historial de Libros</span>
                        </a>
                        <ul>
                            <li class="active">
                                <a href="<?php print path("biblioteca/libros_prestados_usuario/") ?>"><span>Todos</span></a>
                            </li>

                            <li>
                                <a href="<?php print path("biblioteca/libros_prestados_usuario_prestados/") ?>"><span>Prestados</span></a>
                            </li>

                            <li>
                                <a href="<?php print path("biblioteca/libros_prestados_usuario_entregados/") ?>"><span>Entregados</span></a>
                            </li>

                        </ul>
                    </li>
                       <li>
                        <?php if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="bibliotecario")){ ?>
                        <a href="<?php print path("biblioteca/libros_prestar/") ?>">
                            <i class="entypo-book"></i>
                            <span>Prestar Libro</span>
                        </a>
                        <?php }else{ ?>
                        <a href="<?php print path("biblioteca/libros_reservar/") ?>">
                            <i class="entypo-book"></i>
                            <span>Reservar Libro</span>
                        </a>
                        <?php } ?>
                    </li>
                    <?php if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="bibliotecario")){ ?>
                    <li>
                        <a>
                            <i class="entypo-info"></i>
                            <span>Libros Prestados</span>
                        </a>
                        <ul>
                            <li class="active">
                                <a href="<?php print path("biblioteca/libros_prestados_todos/") ?>"><span>Todos</span></a>
                            </li>

                            <li>
                                <a href="<?php print path("biblioteca/libros_prestados/") ?>"><span>Libros</span></a>
                            </li>

                            <li>
                                <a href="<?php print path("biblioteca/libros_prestados_personas/") ?>"><span>Personas</span></a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a>
                            <i class="entypo-box"></i>
                            <span>Inventario Libros</span>
                        </a>
                        <ul>
                            <li class="active">
                                <a href="<?php print path("biblioteca/libros_todos/") ?>"><span>Todos</span></a>
                            </li>
                            <li>
                                <a href="<?php print path("biblioteca/libros_agregar/") ?>"><span>Agregar</span></a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php print path("biblioteca/listas_usuarios/") ?>">
                            <i class="entypo-users"></i>
                            <span>Lista de Usuarios</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php print path("biblioteca/listas_mensajes/") ?>">
                            <i class="entypo-mail"></i>
                            <span>Mensajes Internos</span>
                        </a>
                    </li>
                    <li>
                        <a>
                            <i class="entypo-docs"></i>
                            <span>Reportes</span>
                        </a>
                        <ul>
                            <li class="active">
                                <a target="_BLANK" href="<?php print path("biblioteca/reporte_usuarios_global/") ?>"><span>Usuarios</span></a>
                            </li>
                            <li class="active">
                                <a target="_BLANK" href="<?php print path("biblioteca/reporte_libro_clasificacion/") ?>"><span>Clasificacion</span></a>
                            </li>
							<li>
                                <a target="_BLANK" href="<?php print path("biblioteca/reporte_archivos_global/") ?>"><span>Inventario</span></a>
                            </li>
                            <?php if(SESSION("tipo_user")=="admin"){?>
                            <li>
                                <a target="_BLANK" href="<?php print path("biblioteca/reporte_logs_global/") ?>"><span>Logs</span></a>
                            </li>
                            <?php } ?>
                        </ul>
                    </li>
                    <?php }?>
                    <?php if(SESSION("tipo_user")=="admin"){ ?>
                    <li>
                        <a>
                            <i class="entypo-database"></i>
                            <span>Configuracion</span>
                        </a>
                        <ul>
                            <li class="active">
                                <a href="#"><span>Plantillas</span></a>
                                <ul>
                                    <li>
                                        <a href="<?php print path("biblioteca/plantillas_agregar/") ?>">Agregar</a>
                                    </li>
                                    <li>
                                        <a href="<?php print path("biblioteca/plantillas_todos/") ?>">Ver Todas</a>
                                    </li>
                                </ul>   
                            </li>
                            <li>
                                <a href="<?php print path("biblioteca/listas_informacion/") ?>"><span>Informacion</span></a>
                            </li>
                            <li class="active">
                                <a href="<?php print path("biblioteca/datos_ver/") ?>"><span>Sistema</span></a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a>
                            <i class="entypo-drive"></i>
                            <span>Administracion</span>
                        </a>
                        <ul>
                            <li class="active">
                                <a href="<?php print path("biblioteca/respaldar_bd/") ?>"><span>Respaldar BD</span></a>
                            </li>
                            <li>
                                <a href="<?php print path("biblioteca/respaldar_files/") ?>"><span>Respaldar Ficheros</span></a>
                            </li>
                        </ul>
                    </li>
                    <?php }?>
                    <li>
                        <a href="<?php print path("biblioteca/salir/") ?>">
                            <i class="entypo-logout"></i>
                            <span>Salir</span>
                        </a>
                    </li>
                </ul>
                        
            </div>  
            <div class="main-content">

                <div class="row">    
                    <!-- Profile Info and Notifications -->
                    <div class="col-md-6 col-sm-8 clearfix">
                        <ul class="user-info pull-left pull-none-xsm">
                            <!-- Profile Info -->
                            <li class="profile-info dropdown">
                                <!-- add class "pull-right" if you want to place this from right -->
                                <a href="l#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php print $this->themePath; ?>/images/<?php print SESSION('ruta_img'); ?>" class="img-circle" width="44" />
                                    <?php echo SESSION("nombre").", ".SESSION("apellido");?>
                                </a>
                                
                                <ul class="dropdown-menu">
                                    <!-- Reverse Caret -->
                                    <li class="caret"></li>
                                    <!-- Profile sub-links -->
                                    <li>
                                        <a href="<?php print path("biblioteca/perfil_usuario/"); ?>">
                                            <i class="entypo-user"></i>
                                            Perfil Usuario
                                        </a>
                                    </li>
                                    <li>
                                        <a target="_BLANK" href="<?php print path("biblioteca/perfil_carnet/"); ?>">
                                            <i class="entypo-user"></i>
                                            Generar Carnet
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>        
                    <!-- Raw Links -->
                    <div class="col-md-6 col-sm-4 clearfix hidden-xs">
                        <ul class="list-inline links-list pull-right">
                            <!-- <li class="sep"></li><li>
                                <a href="index.html#" data-toggle="chat" data-animate="1" data-collapse-sidebar="1">
                                    <i class="entypo-chat"></i>
                                    Chat
                                    <span class="badge badge-success chat-notifications-badge is-hidden">0</span>
                                </a>
                            </li>-->
                            <li class="sep"></li>
                            
                            <li>
                                <a href="<?php print path("biblioteca/salir/") ?>">
                                    Salir <i class="entypo-logout right"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <?php }?>